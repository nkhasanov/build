@echo off

@rem Windows Java launcher is based on WinRun4j: http://winrun4j.sourceforge.net/
@rem See the site to know about building the launcher.
@rem Current version: 0.4.5 (2013-04-14)

@rem Shortly, you need to run the following sequence of commands in order to update exe file.
@rem This is possible only on Windows :)

@rem ========== 32 bit ==========

echo Cleanup resources before start
RCEDIT.exe /C fa13-jogador.exe

echo Update embedded settings
RCEDIT.exe /N fa13-jogador.exe fa13-jogador-embedded.ini

echo Update icon file
RCEDIT.exe /I fa13-jogador.exe fa13-jogador.ico

echo Update splash image
RCEDIT.exe /S fa13-jogador.exe splash.jpg

echo Print out current content
RCEDIT.exe /L fa13-jogador.exe
