package gradle.plugin.jre

class JreVersionTest extends GroovyTestCase {

    public void testGetBuild() throws Exception {
        assertEquals('01', new JreVersion(7, 1, 1).getBuild())
    }}
