package gradle.plugin.jre

enum Platform {
    WINDOWS('windows'),
    LINUX('linux'),
    MAC('macosx')

    private final String name;

    Platform(String name) {
        this.name = name
    }

    String getName() {
        return name
    }
}
