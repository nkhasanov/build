package gradle.plugin.jre

class JreVersion implements Serializable {
    private final int major
    private final int update
    private final int build

    JreVersion(int major, int update, int build) {
        this.major = major
        this.update = update
        this.build = build
    }

    int getMajor() {
        return major
    }

    int getUpdate() {
        return update
    }

    String getBuild() {
        String.format("%02d", build)
    }

    boolean equals(o) {
        if (this.is(o)) return true
        if (!(o instanceof JreVersion)) return false

        JreVersion that = (JreVersion) o

        if (build != that.build) return false
        if (major != that.major) return false
        if (update != that.update) return false

        return true
    }

    int hashCode() {
        int result
        result = major
        result = 31 * result + update
        result = 31 * result + build
        return result
    }
}
