package gradle.plugin.jre

enum Architecture {
    X86('i586'),
    X64('x64')

    private final String name;

    Architecture(String name) {
        this.name = name
    }

    String getName() {
        return name
    }
}
