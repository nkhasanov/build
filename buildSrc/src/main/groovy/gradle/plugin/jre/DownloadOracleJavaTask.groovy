package gradle.plugin.jre

import org.apache.http.client.methods.HttpGet
import org.apache.http.impl.client.BasicCookieStore
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.apache.http.impl.cookie.BasicClientCookie
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

import javax.inject.Inject

class DownloadOracleJavaTask extends DefaultTask  {
    private static final BASE_URI = 'http://download.oracle.com/otn-pub/java/jdk'

    JreVersion version
    Platform platform
    Architecture arch

    @Inject
    DownloadOracleJavaTask() {
    }

    @OutputFile
    File getOutputFile() {
        return project.file("$temporaryDir/$jreFileName")
    }

    @Input
    String getSourceUri() {
        'http://repository.fa13.info/distrib/fa13-jogador-linux.zip'
        "${BASE_URI}/${version.major}u${version.update}-b${version.build}/${jreFileName}"
    }

    String getJreFileName() {
        "jre-${version.major}u${version.update}-${platform.name}-${arch.name}.tar.gz"
    }

    @TaskAction
    void downloadJre() {
        logger.info("Downloading $sourceUri to ${outputFile.absolutePath}")
        httpClient().withCloseable { httpClient ->
            httpClient.execute(new HttpGet(sourceUri)).withCloseable { response ->
                outputFile.withOutputStream { out ->
                    response.entity.writeTo(out)
                }
            }
        }
    }

    private static CloseableHttpClient httpClient() {
        def cookieStore = new BasicCookieStore()
        cookieStore.addCookie(cookie('gpw_e24', 'http://www.oracle.com/'))
        cookieStore.addCookie(cookie('oraclelicense', 'accept-securebackup-cookie'))
        HttpClientBuilder.create().setDefaultCookieStore(cookieStore).build()
    }

    private static BasicClientCookie cookie(String name, String value) {
        def cookie = new BasicClientCookie(name, value)
        cookie.setVersion(0);
        cookie.setDomain('.oracle.com');
        cookie.setPath('/');
        cookie
    }

}