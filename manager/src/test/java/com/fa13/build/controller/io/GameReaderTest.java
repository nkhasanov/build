package com.fa13.build.controller.io;


import com.fa13.build.model.All;
import com.fa13.build.model.Team;
import com.fa13.build.model.game.GameForm;
import com.fa13.build.utils.GameFormBuilder;
import com.fa13.build.utils.ResourceUtils;
import com.fa13.build.view.MainWindow;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.matchers.apachecommons.ReflectionEquals;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({MainWindow.class})
public class GameReaderTest {

    @Before
    public void setUp() {
        mockStatic(MainWindow.class);
        All all = mock(All.class);
        Team teamMy = mock(Team.class);
        Team teamG25 = mock(Team.class);

        when(MainWindow.getAllInstance()).thenReturn(all);
        when(all.getTeamByName("Моя команда")).thenReturn(teamMy);
        when(all.getTeamByName("Гурия")).thenReturn(teamG25);
        when(teamMy.getId()).thenReturn("MY");
        when(teamG25.getId()).thenReturn("G25");
    }

    @Test
    public void readGameFormFile() throws Exception {
        String formFileName = "/forms/game-full-input.f13";
        String formFile = ResourceUtils.getClasspathResourceAbsolutePath(formFileName);

        GameForm gameForm = GameReader.readGameFormFile(formFile);

        assertThat(gameForm, new ReflectionEquals(GameFormBuilder.createGameForm()));
    }

    @Test
    public void readGameFormFile_legacyBuild() throws Exception {
        String formFileName = "/forms/game-legacy.f13";
        String formFile = ResourceUtils.getClasspathResourceAbsolutePath(formFileName);

        GameForm gameForm = GameReader.readGameFormFile(formFile);

        assertThat(gameForm, new ReflectionEquals(GameFormBuilder.createLegacyGameForm()));
    }
}