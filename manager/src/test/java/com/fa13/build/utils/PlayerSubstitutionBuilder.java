package com.fa13.build.utils;


import com.fa13.build.model.PlayerAmplua;
import com.fa13.build.model.game.PlayerPosition;
import com.fa13.build.model.game.PlayerSubstitution;

public class PlayerSubstitutionBuilder {
    private PlayerSubstitution substitution;

    private PlayerSubstitutionBuilder(int time, int minDifference, int maxDifference) {
        substitution = new PlayerSubstitution();
        substitution.setTime(time);
        substitution.setMinDifference(minDifference);
        substitution.setMaxDifference(maxDifference);
    }

    public static PlayerSubstitutionBuilder substitution(int time, int minDifference, int maxDifference) {
        return new PlayerSubstitutionBuilder(time, minDifference, maxDifference);
    }

    public PlayerSubstitutionBuilder out(int outPlayerNumber) {
        substitution.setSubstitutedPlayer(outPlayerNumber);
        return this;
    }

    public PlayerSubstitutionBuilder in(int playerNumber) {
        substitution.setNumber(playerNumber);
        substitution.setAmplua(null);
        substitution.setUseCoordinates(false);
        return this;
    }

    public PlayerSubstitutionBuilder in(int playerNumber, PlayerAmplua amplua) {
        substitution.setNumber(playerNumber);
        substitution.setAmplua(amplua);
        substitution.setUseCoordinates(false);
        return this;
    }

    public PlayerSubstitutionBuilder in(PlayerPosition playerPosition) {
        substitution.setNumber(playerPosition.getNumber());
        substitution.setAmplua(null);
        substitution.setUseCoordinates(true);

        substitution.setDefenceX(playerPosition.getDefenceX());
        substitution.setDefenceY(playerPosition.getDefenceY());
        substitution.setAttackX(playerPosition.getAttackX());
        substitution.setAttackY(playerPosition.getAttackY());
        substitution.setFreekickX(playerPosition.getFreekickX());
        substitution.setFreekickY(playerPosition.getFreekickY());
        substitution.setSpecialRole(playerPosition.getSpecialRole());
        substitution.setPenaltyOrder(playerPosition.getPenaltyOrder());
        substitution.setLongShot(playerPosition.isLongShot());
        substitution.setActOnFreekick(playerPosition.getActOnFreekick());
        substitution.setFantasista(playerPosition.isFantasista());
        substitution.setDispatcher(playerPosition.isDispatcher());
        substitution.setPersonalDefence(playerPosition.getPersonalDefence());
        substitution.setPressing(playerPosition.isPressing());
        substitution.setKeepBall(playerPosition.isKeepBall());
        substitution.setDefenderAttack(playerPosition.isDefenderAttack());
        substitution.setHardness(playerPosition.getHardness());
        substitution.setPassingStyle(playerPosition.getPassingStyle());
        substitution.setGoalkeeper(playerPosition.isGoalkeeper());
        return this;
    }

    public PlayerSubstitution get() {
        return substitution;
    }
}