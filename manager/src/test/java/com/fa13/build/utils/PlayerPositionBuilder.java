package com.fa13.build.utils;


import com.fa13.build.model.game.PassingStyle;
import com.fa13.build.model.game.PlayerPosition;
import com.fa13.build.model.game.Hardness;

import static com.fa13.build.model.game.FreekickAction.FK_DEFAULT;
import static com.fa13.build.model.game.FreekickAction.FK_NO;
import static com.fa13.build.model.game.FreekickAction.FK_YES;

public class PlayerPositionBuilder {
    private PlayerPosition player;

    private PlayerPositionBuilder(int number) {
        player = new PlayerPosition();
        player.setNumber(number);
    }

    public static PlayerPositionBuilder abstractPlayer(int number) {
        return new PlayerPositionBuilder(number);
    }

    public static PlayerPositionBuilder goalkeeper(int number) {
        PlayerPositionBuilder builder = new PlayerPositionBuilder(number);
        builder.player.setGoalkeeper(true);
        return builder;
    }

    public static PlayerPositionBuilder leftDefence(int number) {
        PlayerPositionBuilder builder = new PlayerPositionBuilder(number);
        builder.player.setGoalkeeper(false);
        builder.player.setDefenceX(90);
        builder.player.setDefenceY(100);
        builder.player.setAttackX(315);
        builder.player.setAttackY(100);
        return builder;
    }

    public static PlayerPositionBuilder centralDefence(int number) {
        PlayerPositionBuilder builder = new PlayerPositionBuilder(number);
        builder.player.setGoalkeeper(false);
        builder.player.setDefenceX(90);
        builder.player.setDefenceY(225);
        builder.player.setAttackX(315);
        builder.player.setAttackY(225);
        return builder;
    }

    public static PlayerPositionBuilder rightDefence(int number) {
        PlayerPositionBuilder builder = new PlayerPositionBuilder(number);
        builder.player.setGoalkeeper(false);
        builder.player.setDefenceX(90);
        builder.player.setDefenceY(350);
        builder.player.setAttackX(315);
        builder.player.setAttackY(350);
        return builder;
    }

    public static PlayerPositionBuilder leftMidfield(int number) {
        PlayerPositionBuilder builder = new PlayerPositionBuilder(number);
        builder.player.setGoalkeeper(false);
        builder.player.setDefenceX(200);
        builder.player.setDefenceY(100);
        builder.player.setAttackX(450);
        builder.player.setAttackY(100);
        return builder;
    }

    public static PlayerPositionBuilder centralMidfield(int number) {
        PlayerPositionBuilder builder = new PlayerPositionBuilder(number);
        builder.player.setGoalkeeper(false);
        builder.player.setDefenceX(200);
        builder.player.setDefenceY(225);
        builder.player.setAttackX(450);
        builder.player.setAttackY(225);
        return builder;
    }

    public static PlayerPositionBuilder rightMidfield(int number) {
        PlayerPositionBuilder builder = new PlayerPositionBuilder(number);
        builder.player.setGoalkeeper(false);
        builder.player.setDefenceX(200);
        builder.player.setDefenceY(350);
        builder.player.setAttackX(450);
        builder.player.setAttackY(350);
        return builder;
    }

    public static PlayerPositionBuilder leftForward(int number) {
        PlayerPositionBuilder builder = new PlayerPositionBuilder(number);
        builder.player.setGoalkeeper(false);
        builder.player.setDefenceX(300);
        builder.player.setDefenceY(100);
        builder.player.setAttackX(540);
        builder.player.setAttackY(100);
        return builder;
    }

    public static PlayerPositionBuilder centralForward(int number) {
        PlayerPositionBuilder builder = new PlayerPositionBuilder(number);
        builder.player.setGoalkeeper(false);
        builder.player.setDefenceX(300);
        builder.player.setDefenceY(225);
        builder.player.setAttackX(540);
        builder.player.setAttackY(225);
        return builder;
    }

    public static PlayerPositionBuilder rightForward(int number) {
        PlayerPositionBuilder builder = new PlayerPositionBuilder(number);
        builder.player.setGoalkeeper(false);
        builder.player.setDefenceX(300);
        builder.player.setDefenceY(350);
        builder.player.setAttackX(540);
        builder.player.setAttackY(350);
        return builder;
    }

    public PlayerPositionBuilder defencePosition(int x, int y) {
        player.setDefenceX(x);
        player.setDefenceY(y);
        return this;
    }

    public PlayerPositionBuilder attackPosition(int x, int y) {
        player.setAttackX(x);
        player.setAttackY(y);
        return this;
    }

    public PlayerPositionBuilder freekickPosition(int x, int y) {
        player.setFreekickX(x);
        player.setFreekickY(y);
        return this;
    }

    public PlayerPositionBuilder captain() {
        player.setCaptain(true);
        return this;
    }

    public PlayerPositionBuilder assistantCaptain() {
        player.setCaptainAssistant(true);
        return this;
    }

    public PlayerPositionBuilder penalty() {
        player.setPenalty(true);
        return this;
    }

    public PlayerPositionBuilder assistantPenalty() {
        player.setPenaltyAssistant(true);
        return this;
    }

    public PlayerPositionBuilder freeKick() {
        player.setDirectFreekick(true);
        return this;
    }

    public PlayerPositionBuilder assistantFreeKick() {
        player.setDirectFreekickAssistant(true);
        return this;
    }

    public PlayerPositionBuilder indirectFreeKick() {
        player.setIndirectFreekick(true);
        return this;
    }

    public PlayerPositionBuilder assistantIndirectFreeKick() {
        player.setIndirectFreekickAssistant(true);
        return this;
    }

    public PlayerPositionBuilder leftCorner() {
        player.setLeftCorner(true);
        return this;
    }

    public PlayerPositionBuilder assistantLeftCorner() {
        player.setLeftCornerAssistant(true);
        return this;
    }

    public PlayerPositionBuilder rightCorner() {
        player.setRightCorner(true);
        return this;
    }

    public PlayerPositionBuilder assistantRightCorner() {
        player.setRightCornerAssistant(true);
        return this;
    }

    public PlayerPositionBuilder penaltyOrder(int order) {
        player.setPenaltyOrder(order);
        return this;
    }

    public PlayerPositionBuilder personalDefence(int playerNumber) {
        player.setPersonalDefence(playerNumber);
        return this;
    }

    public PlayerPositionBuilder passingStyle(PassingStyle style) {
        player.setPassingStyle(style);
        return this;
    }

    public PlayerPositionBuilder hardness(Hardness hardness) {
        player.setHardness(hardness);
        return this;
    }

    public PlayerPositionBuilder actOnFreeKick(Boolean value) {
        if (value == null) {
            player.setActOnFreekick(FK_DEFAULT);
        } else {
            player.setActOnFreekick(value ? FK_YES : FK_NO);
        }
        return this;
    }

    public PlayerPositionBuilder personalSettings(char[] settings) {
        for (char setting : settings) {
            switch (setting) {
                case 'У':
                    player.setLongShot(true);
                    break;
                case 'Х':
                    player.setFantasista(true);
                    break;
                case 'Д':
                    player.setDispatcher(true);
                    break;
                case 'П':
                    player.setPressing(true);
                    break;
                case 'М':
                    player.setKeepBall(true);
                    break;
                case 'З':
                    player.setDefenderAttack(true);
                    break;
            }
        }
        return this;
    }

    public PlayerPosition get() {
        player.validatePersonalSettings();
        return player;
    }
}