package com.fa13.build.api;

public interface IDisposable {

    public void dispose();
    
}