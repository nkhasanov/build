package com.fa13.build.api;

import java.util.Properties;

public interface ILoadable {

    public void load(Properties props);
    
}