package com.fa13.build.controller.io;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fa13.build.model.Player;
import com.fa13.build.model.PlayerAmplua;
import com.fa13.build.model.TransferList;
import com.fa13.build.model.TransferPlayer;

public class TransferListReader {
    private static int transferID;

    public static TransferList readTransferListFile(String filename) throws ReaderException {
        try {
            File transferListFile = new File(filename);
            InputStream transferListStream = new FileInputStream(transferListFile);
            BufferedReader reader = null;
            try {
                reader = new BufferedReader(new InputStreamReader(transferListStream, "Cp1251"));
            } catch (UnsupportedEncodingException e) {
                System.err.println("Error while encoding transfer list file");
            }
            TransferList tlist = readTransferList(reader);
            reader.close();
            return tlist;
        } catch (ReaderException e) {
            throw e;
        } catch (Exception e) {
            throw new ReaderException("Unable to open transfer list file: " + filename);
        }
    }

    public static TransferList readTransferList(BufferedReader reader) throws ReaderException {
        try {
            String s = reader.readLine();
            if (s.compareTo("format=1") != 0) {
                System.err.println("Inappropriate file format");
                return null;
            }
            s = reader.readLine();
            SimpleDateFormat dateParser = new SimpleDateFormat("dd.MM.yyyy");
            Date date = null;
            try {
                date = dateParser.parse(s);
            } catch (ParseException e) {
                System.err.println("Inappropriate date format");
                return null;
            }
            transferID = 1;
            List<TransferPlayer> players = new ArrayList<TransferPlayer>();
            TransferPlayer curr = readTransferPlayer(reader);
            while (curr != null) {
                players.add(curr);
                curr = readTransferPlayer(reader);
            }
            transferID = 1;
            return new TransferList(date, players);
        } catch (Exception e) {
            throw new ReaderException("Incorrect file format");
        }
    }

    public static TransferPlayer readTransferPlayer(BufferedReader reader) throws ReaderException {
        try {
            String s = reader.readLine();
            if (s == null || s.trim().isEmpty()) {
                return null;
            }
            String[] parsedData = s.split("/");
            if (parsedData.length == 1) {
                return null;
            }
            int pos = 0;
            int id = Integer.valueOf(parsedData[pos++]);
            String name = parsedData[pos++];
            String nationality = parsedData[pos++];
            String nationalityCode = Player.NATIONALITIES.get(nationality);
            String previousTeam = parsedData[pos++];
            PlayerAmplua position = PlayerAmplua.resolveByFormValue(parsedData[pos++]);
            int age = Integer.valueOf(parsedData[pos++]);
            int talent = Integer.valueOf(parsedData[pos++]);
            int salary = Integer.valueOf(parsedData[pos++]);
            int strength = Integer.valueOf(parsedData[pos++]);
            int health = Integer.valueOf(parsedData[pos++]);
            String abilities = parsedData[pos++];
            int price = Integer.valueOf(parsedData[pos++]);
            int bids = 0;
            TransferPlayer transferPlayer = new TransferPlayer(id, name, nationalityCode, previousTeam, position, age, talent, salary, strength, health, abilities, price, bids, transferID++);
            
            transferPlayer.setShooting(Integer.valueOf(parsedData[pos++]));
            transferPlayer.setHandling(Integer.valueOf(parsedData[pos++]));
            transferPlayer.setReflexes(Integer.valueOf(parsedData[pos++]));
            transferPlayer.setPassing(Integer.valueOf(parsedData[pos++]));
            transferPlayer.setCross(Integer.valueOf(parsedData[pos++]));
            transferPlayer.setDribbling(Integer.valueOf(parsedData[pos++]));
            transferPlayer.setTackling(Integer.valueOf(parsedData[pos++]));
            transferPlayer.setHeading(Integer.valueOf(parsedData[pos++]));
            transferPlayer.setSpeed(Integer.valueOf(parsedData[pos++]));
            transferPlayer.setStamina(Integer.valueOf(parsedData[pos++]));
            transferPlayer.setBirthtour(Integer.valueOf(parsedData[pos++]));
            return transferPlayer;
        } catch (Exception e) {
            throw new ReaderException("Incorrect file format");
        }
    }
}
