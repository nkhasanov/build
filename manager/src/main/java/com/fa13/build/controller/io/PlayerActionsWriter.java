package com.fa13.build.controller.io;

import com.fa13.build.model.PlayerActions;
import com.fa13.build.model.PlayerBid;
import com.fa13.build.protect.ProtectionUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class PlayerActionsWriter {

    public static void writePlayerActionsFile(String filename, PlayerActions actions) throws IOException {
        HashingOutputStream stream = new HashingOutputStream(new FileOutputStream(filename));
        writePlayerActions(stream, actions);
        stream.close();
    }

    public static void writePlayerActions(OutputStream stream, PlayerActions actions) throws IOException {
        String requestContent = createRequestContent(actions);

        stream.write(requestContent.getBytes(HashingOutputStream.WIN_CHARSET));
        stream.write(("*****" + AbstractFormWriter.LINE_BREAK).getBytes(HashingOutputStream.WIN_CHARSET));
        stream.write(ProtectionUtils.getDefenceData(requestContent, HashingOutputStream.WIN_CHARSET, getAdditionalDefenceValues(actions)));
    }

    private static String createRequestContent(PlayerActions actions) {
        StringBuilder requestBuilder = new StringBuilder();
        requestBuilder.append("Заявка составлена с помощью BuildJava-1.0").append(AbstractFormWriter.LINE_BREAK);
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("M/d/yyyy H:m:s");
        requestBuilder.append("LocalTime = ").append(sdf.format(date)).append(AbstractFormWriter.LINE_BREAK);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        requestBuilder.append("UTCTime = ").append(sdf.format(date)).append(AbstractFormWriter.LINE_BREAK);

        requestBuilder.append(AbstractFormWriter.LINE_BREAK);
        requestBuilder.append(actions.getPassword() == null ? "" : actions.getPassword()).append(AbstractFormWriter.LINE_BREAK);

        for (PlayerBid bid : actions.getBids()) {
            if (bid.getNumber()>0 && !bid.isEmpty())
                requestBuilder.append(bid.toString()).append(AbstractFormWriter.LINE_BREAK);
        }

        return requestBuilder.toString();
    }
    
    protected static String[] getAdditionalDefenceValues(PlayerActions form) {
        return new String[] {
            form.getTeamID()
        };
    }
    
}
