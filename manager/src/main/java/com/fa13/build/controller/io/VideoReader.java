package com.fa13.build.controller.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.fa13.build.model.Video;
import com.fa13.build.model.VideoBall;
import com.fa13.build.model.VideoFrame;
import com.fa13.build.model.VideoPlayer;
import com.fa13.build.model.VideoPlayerInfo;
import com.fa13.build.model.VideoTeamsInfo;

public class VideoReader {
    public static Video readVideo(String fname) throws ReaderException {
        try {
            File videoFile = new File(fname);
            InputStream videoStream = null;
            InputStream infoStream = null;
            if (fname.endsWith(".zip")) {
                String videoName = videoFile.getName();
                videoName = videoName.substring(0, videoName.length() - 4)
                        .concat(".gr");
                ZipFile allZip = new ZipFile(videoFile);
                Enumeration<? extends ZipEntry> ff = allZip.entries();
                while (ff.hasMoreElements()) {
                    ZipEntry ze = ff.nextElement();
                    if (ze.getName().endsWith(".gr")) {
                        videoStream = allZip.getInputStream(new ZipEntry(ze));
                    } else if (ze.getName().endsWith(".txt")) {
                        infoStream = allZip.getInputStream(new ZipEntry(ze));
                    }
                }

            } else {
                return null;
            }

            Video video = parseInputStream(videoStream);
            addVideoInfo(video, infoStream);

            videoStream.close();
            infoStream.close();
            return video;
        } catch (Exception e) {
            throw new ReaderException("Unable to open video file: " + fname);
        }
    }

    private static void addVideoInfo(Video video, InputStream infoStream)
            throws IOException {
        BufferedReader infoReader = null;
        try {
            infoReader = new BufferedReader(new InputStreamReader(infoStream,
                    "Cp1251"));
        } catch (UnsupportedEncodingException e) {
            System.err.println("Error while encoding all file");
        }
        StringBuilder teamsInfo = new StringBuilder(3000);

        infoReader.readLine();
        infoReader.readLine();//город и стадион, кажеться
        infoReader.readLine();
        infoReader.readLine();//vs
        infoReader.readLine();

        VideoTeamsInfo vti = new VideoTeamsInfo();

        String infoLine = infoReader.readLine();
        infoLine = infoLine.substring(infoLine.indexOf('(') + 1);
        for (int index = 0; index < 10; index++) {
            vti.homePlayers.add(new VideoPlayerInfo());
            vti.homePlayers.get(index).setInfo(infoLine.substring(0, infoLine.indexOf(',')));
            infoLine = infoLine.substring(infoLine.indexOf(',') + 1);
        }
        vti.homePlayers.add(new VideoPlayerInfo());
        vti.homePlayers.get(10).setInfo(infoLine.substring(0, infoLine.indexOf(';')));
        infoLine = infoLine.substring(infoLine.indexOf(':') + 1);
        for (int index = 11; index < 17; index++) {
            vti.homePlayers.add(new VideoPlayerInfo());
            vti.homePlayers.get(index).setInfo(infoLine.substring(0, infoLine.indexOf(',')));
            infoLine = infoLine.substring(infoLine.indexOf(',') + 1);
        }
        vti.homePlayers.add(new VideoPlayerInfo());
        vti.homePlayers.get(17).setInfo(infoLine.substring(0, infoLine.indexOf(')')));

        infoReader.readLine();

        infoLine = infoReader.readLine();
        infoLine = infoLine.substring(infoLine.indexOf('(') + 1);
        for (int index = 0; index < 10; index++) {
            vti.awayPlayers.add(new VideoPlayerInfo());
            vti.awayPlayers.get(index).setInfo(infoLine.substring(0, infoLine.indexOf(',')));
            infoLine = infoLine.substring(infoLine.indexOf(',') + 1);
        }
        vti.awayPlayers.add(new VideoPlayerInfo());
        vti.awayPlayers.get(10).setInfo(infoLine.substring(0, infoLine.indexOf(';')));
        infoLine = infoLine.substring(infoLine.indexOf(':') + 1);
        for (int index = 11; index < 17; index++) {
            vti.awayPlayers.add(new VideoPlayerInfo());
            vti.awayPlayers.get(index).setInfo(infoLine.substring(0, infoLine.indexOf(',')));
            infoLine = infoLine.substring(infoLine.indexOf(',') + 1);
        }
        vti.awayPlayers.add(new VideoPlayerInfo());
        vti.awayPlayers.get(17).setInfo(infoLine.substring(0, infoLine.indexOf(')')));

        while (!infoReader.readLine().equals("*")) {
        }

        for (int index = 0; index < 18; index++) {
            infoLine = infoReader.readLine();
            vti.homePlayers.get(index).setNumber(infoLine);
        }

        for (int index = 0; index < 18; index++) {
            infoLine = infoReader.readLine();
            vti.awayPlayers.get(index).setNumber(infoLine);
        }


        video.setTeamsInfo(vti);
    }

    private static Video parseInputStream(InputStream videoReader)
            throws IOException {
        Video resultVideo = new Video();
        int tempVideoFrameTimeInSec = 0;
        VideoFrame tempVideoFrame = new VideoFrame(tempVideoFrameTimeInSec);
        videoReader.read();
        int code = videoReader.read();
        int[][] a = new int[23][3];
        int curr = 0;
        List<Byte> raw = new ArrayList<Byte>();
        while ((curr = videoReader.read()) != -1) {
            raw.add((byte) (curr ^ code));
        }
        raw.remove(raw.size() - 1);
        int cnt = 0;
        for (Iterator<Byte> iter = raw.iterator(); iter.hasNext();) {
            cnt++;
            int currentVideoPrimitiveNumber = 1;
            for (int i = 0; i < 23; i++) {
                for (int j = 0; j < 2; j++) {
                    boolean loop = true;
                    while (loop && iter.hasNext()) {
                        a[i][j] = iter.next();
                        switch (a[i][j]) {
                            case 100:
                            case 101:
                            case 102:
                            case 103:
                            case 104:
                            case 105:
                            case 106:
                            case 107:
                            case 111: {
                                iter.next();
                                iter.next();
                                break;
                            }
                            case 108: {
                                tempVideoFrame.addAdditionalInfo(String.format("%5d [%2d %1d] turn\n", cnt, i, j));
                                iter.next();
                                iter.next();
                                break;
                            }
                            case 109: {
                                tempVideoFrame.addAdditionalInfo(String.format("%5d [%2d %1d] crossbar\n", cnt, i, j));
                                iter.next();
                                iter.next();
                                break;
                            }
                            case 110: {
                                tempVideoFrame.addAdditionalInfo(String.format("%5d [%2d %1d] post\n", cnt, i, j));
                                iter.next();
                                iter.next();
                                break;
                            }
                            case 112: {
                                tempVideoFrame.addAdditionalInfo(String.format("%5d [%2d %1d] heading\n", cnt, i, j));
                                iter.next();
                                iter.next();
                                break;
                            }
                            case 113: {
                                tempVideoFrame.addAdditionalInfo(String.format("%5d [%2d %1d] tackle\n", cnt, i, j));
                                iter.next();
                                iter.next();
                                break;
                            }
                            case 114: {
                                tempVideoFrame.addAdditionalInfo(String.format("%5d [%2d %1d] end\n", cnt, i, j));
                                iter.next();
                                break;
                            }
                            case 115: {
                                tempVideoFrame.addAdditionalInfo(String.format("%5d [%2d %1d] half end\n", cnt, i, j));
                                iter.next();
                                iter.next();
                                break;
                            }
                            case 116: {
                                tempVideoFrame.addAdditionalInfo(String.format("%5d [%2d %1d] shot\n", cnt, i, j));
                                iter.next();
                                iter.next();
                                break;
                            }
                            case 117: {
                                int z = iter.next();
                                tempVideoFrame.addAdditionalInfo(String.format("%5d [%2d %1d] %3d offside\n", cnt, i, j, z));
                                iter.next();
                                break;
                            }
                            case 118: {
                                int z = iter.next();
                                tempVideoFrame.addAdditionalInfo(String.format("%5d [%2d %1d] %3d red\n", cnt, i, j, z));
                                iter.next();
                                break;
                            }
                            case 119: {
                                int z = iter.next();
                                tempVideoFrame
                                        .addAdditionalInfo(String.format("%5d [%2d %1d] %3d yellow\n", cnt, i, j, z));
                                iter.next();
                                break;
                            }
                            case 120: {
                                tempVideoFrame.addAdditionalInfo(String.format("%5d [%2d %1d] free\n", cnt, i, j));
                                iter.next();
                                iter.next();
                                break;
                            }
                            case 121: {
                                int z = iter.next();
                                tempVideoFrame.addAdditionalInfo(String.format("%5d [%2d %1d] %3d injury\n", cnt, i, j, z));
                                iter.next();
                                break;
                            }
                            case 122: {
                                int z = iter.next();
                                int l = iter.next();
                                tempVideoFrame.addAdditionalInfo(String.format("%5d [%2d %1d] %3d %3d substitution\n", cnt, i, j, z, l));
                                break;
                            }
                            case 123: {
                                tempVideoFrame.addAdditionalInfo(String.format("%5d [%2d %1d] out\n", cnt, i, j));
                                iter.next();
                                iter.next();
                                break;
                            }
                            case 124: {
                                tempVideoFrame.addAdditionalInfo(String.format("%5d [%2d %1d] corner\n", cnt, i, j));
                                iter.next();
                                iter.next();
                                break;
                            }
                            case 125: {
                                tempVideoFrame.addAdditionalInfo(String.format("%5d [%2d %1d] penalty\n", cnt, i, j));
                                iter.next();
                                iter.next();
                                break;
                            }
                            case 126: {
                                tempVideoFrame.addAdditionalInfo(String.format("%5d [%2d %1d] freekick\n", cnt, i, j));
                                iter.next();
                                iter.next();
                                break;
                            }
                            case 127: {
                                int z = iter.next();
                                int l = iter.next();
                                tempVideoFrame.addAdditionalInfo(String.format("%5d [%2d %1d] %3d %3d goal\n", cnt, i, j, z, l));
                                break;
                            }
                            default: {
                                loop = false;
                            }
                        }
                    }
                }
                if (currentVideoPrimitiveNumber <= 11) {
                    tempVideoFrame.addHomePlayer(new VideoPlayer(a[i][0], a[i][1]));
                } else if (currentVideoPrimitiveNumber <= 22) {
                    tempVideoFrame.addAwayPlayer(new VideoPlayer(a[i][0], a[i][1]));
                } else {
                    tempVideoFrame.setBall(new VideoBall(a[i][0], a[i][1], a[i][2]));
                }
                currentVideoPrimitiveNumber++;
            }
            resultVideo.addFrame(tempVideoFrame);
            tempVideoFrameTimeInSec += 6;
            tempVideoFrame = new VideoFrame(tempVideoFrameTimeInSec);
        }

        return resultVideo;
    }
}
