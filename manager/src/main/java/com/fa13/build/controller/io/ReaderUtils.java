package com.fa13.build.controller.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class ReaderUtils {

    public static final Charset UTF8_CHARSET = Charset.forName("UTF-8");

    /**
     * reads resource as Properties file delimited by delimiter 
     * @param resource
     * @param delimiter
     * @return
     */
    public static Map<String, String> readProperties(String resource, String delimiter) {

        Map<String, String> resultMap = new HashMap<String, String>();
        BufferedReader br = new BufferedReader(new InputStreamReader(ReaderUtils.class.getResourceAsStream(resource), UTF8_CHARSET));

        try {
            String s = br.readLine();
            while (s != null) {

                String ss[] = s.split(delimiter);
                resultMap.put(ss[0], ss[1]);
                //System.out.println(ss[0] + "=" + ss[1]);
                s = br.readLine();
            }

        } catch (Exception ex) {
            System.out.println(ex.getLocalizedMessage());
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return resultMap;
    }

}