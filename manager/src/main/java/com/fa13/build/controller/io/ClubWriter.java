package com.fa13.build.controller.io;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.fa13.build.model.Club;
import com.fa13.build.model.Club.TriAction;
import com.fa13.build.protect.ProtectionUtils;

public class ClubWriter {

    public static void writeClubFile(String filename, Club club) throws IOException {
        HashingOutputStream stream = new HashingOutputStream(new FileOutputStream(filename));
        writeClub(stream, club);
        stream.close();
    }

    public static void writeClub(OutputStream stream, Club club) throws IOException {
        String requestContent = createRequestContent(club);

        stream.write(requestContent.getBytes(HashingOutputStream.WIN_CHARSET));
        stream.write(("*****" + AbstractFormWriter.LINE_BREAK).getBytes(HashingOutputStream.WIN_CHARSET));
        //stream.write(ProtectionUtils.getDefenceData(requestContent, HashingOutputStream.WIN_CHARSET)); -tested dont accept
        stream.write(ProtectionUtils.getDefenceData(requestContent, HashingOutputStream.WIN_CHARSET, getAdditionalDefenceValues(club)));
    }

    private static String createRequestContent(Club club) {
        StringBuilder requestBuilder = new StringBuilder();
        requestBuilder.append("Заявка составлена с помощью BuildJava-1.0").append(AbstractFormWriter.LINE_BREAK);
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("M/d/yyyy H:m:s");
        requestBuilder.append("LocalTime = ").append(sdf.format(date)).append(AbstractFormWriter.LINE_BREAK);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        requestBuilder.append("UTCTime = ").append(sdf.format(date)).append(AbstractFormWriter.LINE_BREAK);

        requestBuilder.append(AbstractFormWriter.LINE_BREAK);
        requestBuilder.append("parole=");
        requestBuilder.append(club.getPassword() == null ? "" : club.getPassword()).append(AbstractFormWriter.LINE_BREAK);
        /* will not allow edit ! see issue 3 for details https://bitbucket.org/fa13/fa13-build/issue/3/
                String s = club.getEmail();
                if (s != null) {
                    requestBuilder.append("email=").append(s).append(AbstractFormWriter.LINE_BREAK);
                }
                int icq = club.getIcq();
                if (icq != -1) {
                    requestBuilder.append("icq=").append(icq).append(AbstractFormWriter.LINE_BREAK);
                }
        */

        appendBuildingIfSet(requestBuilder, "stadion", club.getStadiumAction(), club.getStadium());
        appendBuildingIfSet(requestBuilder, "basa", club.getSportBaseAction(), club.getSportBase());
        if (club.getSportSchoolAction() != TriAction.TR_NOTHING) {
            //don't allow to delete school as in old build!
            if (!(club.getSportSchoolAction() == TriAction.TR_BUILD && !club.isSchool())) {
                String value = club.getSportSchoolAction() == TriAction.TR_REPAIR ? "remont" : club.isSchool() ? "yes" : "no";
                requestBuilder.append("schoole=").append(value);
                requestBuilder.append(AbstractFormWriter.LINE_BREAK);
            }
        }

        int coachCoefDiff = club.getCoachCoefDiff();
        if (club.isChangeCoachCoef() && coachCoefDiff != 0) {
            requestBuilder.append("KGT=").append(coachCoefDiff).append(AbstractFormWriter.LINE_BREAK);
        }
        if (club.isChangeCoachGK()) {
            appendCoachIfSet(requestBuilder, "gk", club.getCoachGK());
        }
        if (club.isChangeCoachDef()) {
            appendCoachIfSet(requestBuilder, "def", club.getCoachDef());
        }
        if (club.isChangeCoachMid()) {
            appendCoachIfSet(requestBuilder, "mid", club.getCoachMid());
        }
        if (club.isChangeCoachFw()) {
            appendCoachIfSet(requestBuilder, "for", club.getCoachFw());
        }
        if (club.isChangeCoachFitness()) {
            appendCoachIfSet(requestBuilder, "phys", club.getCoachFitness());
        }
        if (club.isChangeCoachMorale()) {
            appendCoachIfSet(requestBuilder, "psy", club.getCoachMorale());
        }

        if (club.isChangeDoctor() && club.getDoctorCount() != -1 && club.getDoctorLevel() != -1) {
            requestBuilder.append("doc=").append(club.getDoctorCount()).append(",").append(club.getDoctorLevel());
            requestBuilder.append(AbstractFormWriter.LINE_BREAK);
        }

        int scoutLevel = club.getScoutLevel();
        if (club.isChangeScout() && scoutLevel != -1) {
            requestBuilder.append("scout=").append(scoutLevel).append(AbstractFormWriter.LINE_BREAK);
        }
        //exchange 2<->3 colors to quick fix
        if (club.isChangeUniformColors()) {
            if (/*club.isChangeColor1() &&*/ club.getColor1() != -1) {
                requestBuilder.append("color1=").append(club.getColor1()).append(AbstractFormWriter.LINE_BREAK);
            }
            if (/*club.isChangeColor3() &&*/ club.getColor3() != -1) {
                requestBuilder.append("color2=").append(club.getColor3()).append(AbstractFormWriter.LINE_BREAK);
            }
            if (/*club.isChangeColor2() &&*/ club.getColor2() != -1) {
                requestBuilder.append("color3=").append(club.getColor2()).append(AbstractFormWriter.LINE_BREAK);
            }

            if (/*club.isChangeColor4() &&*/ club.getColor4() != -1) {
                requestBuilder.append("color4=").append(club.getColor4()).append(AbstractFormWriter.LINE_BREAK);
            }
        }

        return requestBuilder.toString();
    }

    private static void appendBuildingIfSet(StringBuilder requestBuilder, String type, TriAction act, int value) {
        if (act != TriAction.TR_NOTHING) {
            requestBuilder.append(type).append("=").append(act == TriAction.TR_REPAIR ? "remont" : value);
            requestBuilder.append(AbstractFormWriter.LINE_BREAK);
        }
    }

    private static void appendCoachIfSet(StringBuilder requestBuilder, String type, int coach) {
        if (coach > -1) {
            requestBuilder.append(type).append("=").append(coach).append(AbstractFormWriter.LINE_BREAK);
        }
    }

    protected static String[] getAdditionalDefenceValues(Club form) {
        return new String[] { form.getTeamID() };
    }
}
