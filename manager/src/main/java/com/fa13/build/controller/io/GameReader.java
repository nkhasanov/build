package com.fa13.build.controller.io;

import com.fa13.build.model.PlayerAmplua;
import com.fa13.build.model.Team;
import com.fa13.build.model.game.*;
import com.fa13.build.model.game.Tactic;
import com.fa13.build.view.MainWindow;

import java.io.*;

public class GameReader {

    public static GameForm readGameFormFile(String filename) throws ReaderException {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream(filename), "Cp1251"));
            return new GameReader().readGameForm(reader);
        } catch (IOException e) {
            throw new ReaderException("Unable to open game file: " + filename, e);
        }
    }

    public GameForm readGameForm(BufferedReader reader) throws ReaderException {
        GameForm gameForm = GameForm.newInstance();
        try {
            String versionLine = reader.readLine();
            if (!isFormatSupported(versionLine)) {
                throw new ReaderException("Unsupported file format: " + versionLine);
            }
            skipHeaderLines(reader);
        } catch (IOException e) {
            throw new ReaderException("Can't read file", e);
        }

        try {
            gameForm.setTournamentID(reader.readLine());
            gameForm.setGameType(reader.readLine());
            gameForm.setTeamID(teamNameToTeamId(reader.readLine()));
            gameForm.setPassword(reader.readLine());
            reader.readLine(); // skip formation - it doesn't matter for manager app
            gameForm.setStartTactics(readTeamTactics(reader));
            gameForm.setTeamCoachSettings(CoachSettings.resolveByFormValue(reader.readLine()));

            PlayerPosition[] firstTeam = readFirstTeam(reader);
            gameForm.setFirstTeam(firstTeam);

            reader.readLine(); // skip "запасные:"
            gameForm.setBench(readBench(reader));

            reader.readLine(); // skip "стандарты:"
            for (int i = 0; i < 6; i++) {
                parseSpecialRolesLine(firstTeam, reader.readLine());
            }

            reader.readLine(); // skip "пенальти:"
            parsePenaltiesLine(firstTeam, reader.readLine());

            reader.readLine(); // skip "цена билета:"
            gameForm.setPrice(Integer.parseInt(reader.readLine()));

            reader.readLine(); // skip "замены:"
            gameForm.setSubstitutions(readPlayerSubstitutions(reader));
            gameForm.setSubstitutionPreferences(SubstitutionPreferences.resolveByFormValue(Integer.parseInt(reader.readLine())));

            reader.readLine(); // skip "глобал:"
            String[] parsedData = reader.readLine().split(" ");
            gameForm.setDefenceTime(Integer.parseInt(parsedData[0]));
            gameForm.setDefenceMin(Integer.parseInt(parsedData[1]));
            gameForm.setDefenceMax(Integer.parseInt(parsedData[2]));
            parsedData = reader.readLine().split(" ");
            gameForm.setAttackTime(Integer.parseInt(parsedData[0]));
            gameForm.setAttackMin(Integer.parseInt(parsedData[1]));
            gameForm.setAttackMax(Integer.parseInt(parsedData[2]));
            gameForm.setHalftimeChanges(readHalftimeChanges(reader));

            reader.close();
            return gameForm;
        } catch (Exception e) {
            throw new ReaderException("Incorrect file format", e);
        }
    }

    private boolean isFormatSupported(String versionLine) {
        return versionLine != null && (
            versionLine.equals("Заявка составлена с помощью BuildJava-1.0")
                || versionLine.equals("Заявка составлена с помощью Build-1302")
                || versionLine.equals("Заявка составлена с помощью Build-1401")
        );
    }

    private void parseSpecialRolesLine(PlayerPosition[] firstTeam, String line) {
        String[] parsedData = line.split(" ");
        int main = Integer.parseInt(parsedData[0]);
        int assist = Integer.parseInt(parsedData[1]);
        SpecialRole specialRole = SpecialRole.resolveByFormValue(parsedData[2]);
        if (specialRole != null) {
            for (int j = 0; j < 11; j++) {
                if (firstTeam[j].getNumber() == main) {
                    firstTeam[j].setSpecialRole(firstTeam[j].getSpecialRole() | specialRole.getCode());
                }
                if (firstTeam[j].getNumber() == assist) {
                    firstTeam[j].setSpecialRole(firstTeam[j].getSpecialRole() | specialRole.getAssistantCode());
                }
            }
        }
    }

    private void parsePenaltiesLine(PlayerPosition[] firstTeam, String line) {
        String[] parsedData = line.split(" ");
        for (int i = 0; i < parsedData.length; i++) {
            int curr = Integer.parseInt(parsedData[i]);
            for (int j = 0; j < 11; j++) {
                if (firstTeam[j].getNumber() == curr) {
                    firstTeam[j].setPenaltyOrder(i + 1);
                }
            }
        }
    }

    private TeamTactics[] readHalftimeChanges(BufferedReader reader) throws IOException {
        String header;
        String[] parsedData;
        TeamTactics[] halftimeChanges = new TeamTactics[5];
        for (int i = 0; i < 5; i++) {
            header = reader.readLine();
            if (header.equals(GameWriter.UNDEFINED_HALFTIME_CHANGE)) {
                continue;
            }
            parsedData = header.split(" ");
            int minD = Integer.valueOf(parsedData[0]);
            int maxD = Integer.valueOf(parsedData[1]);
            Tactic tc = Tactic.resolveByFormValue(parsedData[2]);
            Hardness hr = Hardness.resolveByFormValue(parsedData.length == 5 ? parsedData[3] : parsedData[3] + " " + parsedData[4]);
            Style st = Style.resolveByFormValue(parsedData.length == 5 ? parsedData[4] : parsedData[5]);
            halftimeChanges[i] = new TeamTactics(tc, hr, st, minD, maxD);
        }
        return halftimeChanges;
    }

    private PlayerSubstitution[] readPlayerSubstitutions(BufferedReader reader) throws ReaderException, IOException {
        PlayerSubstitution[] substitutions = new PlayerSubstitution[25];
        for (int i = 0; i < 25; i++) {
            substitutions[i] = parsePlayerSubstitution(reader.readLine());
        }
        return substitutions;
    }

    private int[] readBench(BufferedReader reader) throws IOException {
        int[] bench = new int[7];
        for (int i = 0; i < 7; i++) {
            bench[i] = Integer.valueOf(reader.readLine());
        }
        return bench;
    }

    private PlayerPosition[] readFirstTeam(BufferedReader reader) throws ReaderException, IOException {
        PlayerPosition[] firstTeam = new PlayerPosition[11];
        for (int i = 0; i < 11; i++) {
            firstTeam[i] = readPlayerPosition(reader.readLine());
        }
        return firstTeam;
    }

    private TeamTactics readTeamTactics(BufferedReader reader) throws IOException {
        Tactic tactic = Tactic.resolveByFormValue(reader.readLine());
        Hardness hardness = Hardness.resolveByFormValue(reader.readLine());
        Style style = Style.resolveByFormValue(reader.readLine());
        return new TeamTactics(tactic, hardness, style, 0, 0);
    }

    private void skipHeaderLines(BufferedReader reader) throws IOException {
        reader.readLine();
        reader.readLine();
        reader.readLine();
    }

    private String teamNameToTeamId(String teamName) {
        Team team = MainWindow.getAllInstance().getTeamByName(teamName);
        if (team == null) {
            // todo: team not found - continue read or interrupt
            return null;
        }
        return team.getId();
    }

    private PlayerPosition readPlayerPosition(String line) throws ReaderException {
        PlayerPosition player = new PlayerPosition();
        try {
            String[] parsedData = line.split(" ");
            player.setGoalkeeper("ВР".equals(parsedData[0]));
            player.setNumber(Integer.parseInt(parsedData[1]));

            player.setDefenceX(readCoordinateX(parsedData[2]));
            player.setDefenceY(readCoordinateY(parsedData[3]));
            player.setAttackX(readCoordinateX(parsedData[4]));
            player.setAttackY(readCoordinateY(parsedData[5]));
            // ignore magic values: -60.0 and 0.0
            player.setFreekickX(readCoordinateX(parsedData[8]));
            player.setFreekickY(readCoordinateY(parsedData[9]));

            readPersonalSettings(player, parsedData, 10);
        } catch (Exception e) {
            throw new ReaderException("Can't read first team player", e);
        }
        return player;
    }

    private PlayerSubstitution parsePlayerSubstitution(String line) throws ReaderException {
        if (line.equals(GameWriter.UNDEFINED_SUBSTITUTION)) {
            return new PlayerSubstitution();
        }
        PlayerSubstitution player = new PlayerSubstitution();
        try {
            String[] parsedData = line.split(" ");
            // ignore magic value: 0
            player.setTime(Integer.parseInt(parsedData[1]));
            player.setMinDifference(Integer.parseInt(parsedData[2]));
            player.setMaxDifference(Integer.parseInt(parsedData[3]));
            player.setSubstitutedPlayer(Integer.parseInt(parsedData[4]));

            player.setNumber(Integer.parseInt(parsedData[5]));
            int specialRoleFormValue = Integer.parseInt(parsedData[6]);
            player.setSpecialRole(specialRoleFormValue < 0 ? 0 : specialRoleFormValue);

            if (parsedData.length == 8) {
                player.setUseCoordinates(false);
                String position = parsedData[7];
                if (position.equals("НН")) {
                    player.setAmplua(null);
                } else {
                    player.setAmplua(PlayerAmplua.resolveByFormValue(position));
                }
            } else {
                player.setUseCoordinates(true);
                player.setDefenceX(readCoordinateX(parsedData[7]));
                player.setDefenceY(readCoordinateY(parsedData[8]));
                player.setAttackX(readCoordinateX(parsedData[9]));
                player.setAttackY(readCoordinateY(parsedData[10]));
                player.setFreekickX(readCoordinateX(parsedData[11]));
                player.setFreekickY(readCoordinateY(parsedData[12]));
                readPersonalSettings(player, parsedData, 13);
            }
        } catch (Exception e) {
            throw new ReaderException("Can't read player substitution", e);
        }
        return player;
    }

    private int readCoordinateY(String s) {
        return Math.round((Float.valueOf(s) + 45) * 5);
    }

    private int readCoordinateX(String s) {
        return Math.round((Float.valueOf(s) + 60) * 5);
    }

    private void readPersonalSettings(PlayerPosition player, String[] parsedData, int startIndex) {
        player.setLongShot(parsedData[startIndex].equals("1"));
        player.setActOnFreekick(FreekickAction.resolveByFormValue(parsedData[startIndex + 1]));
        player.setFantasista(parsedData[startIndex + 2].equals("1"));
        player.setDispatcher(parsedData[startIndex + 3].equals("1"));
        player.setPersonalDefence(Integer.parseInt(parsedData[startIndex + 4]));
        player.setPressing(parsedData[startIndex + 5].equals("1"));
        player.setKeepBall(parsedData[startIndex + 6].equals("1"));
        player.setDefenderAttack(parsedData[startIndex + 7].equals("1"));
        player.setPassingStyle(PassingStyle.resolveByFormValue(parsedData[startIndex + 8]));
        player.setHardness(Hardness.resolveByPlayerFormValue(parsedData[startIndex + 9]));
    }
}
