package com.fa13.build.controller.io;

import com.fa13.build.model.Transfer;
import com.fa13.build.model.TransferBid;
import com.fa13.build.protect.ProtectionUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TransferWriter {

    public static void writeTransferFile(String filename, Transfer transfer) throws IOException {
        HashingOutputStream stream = new HashingOutputStream(new FileOutputStream(filename));
        writeTransfer(stream, transfer);
        stream.close();
    }

    public static void writeTransfer(OutputStream stream, Transfer transfer) throws IOException {
        String requestContent = createRequestContent(transfer);

        stream.write(requestContent.getBytes(HashingOutputStream.WIN_CHARSET));
        stream.write(("*****" + AbstractFormWriter.LINE_BREAK).getBytes(HashingOutputStream.WIN_CHARSET));
        stream.write(ProtectionUtils.getDefenceData(requestContent, HashingOutputStream.WIN_CHARSET, getAdditionalDefenceValues(transfer)));
    }

    private static String createRequestContent(Transfer transfer) throws IOException {
        StringBuilder requestBuilder = new StringBuilder();
        requestBuilder.append("Заявка составлена с помощью BuildJava-1.0").append(AbstractFormWriter.LINE_BREAK);
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("M/d/yyyy H:m:s");
        requestBuilder.append("LocalTime = ").append(sdf.format(date)).append(AbstractFormWriter.LINE_BREAK);
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        requestBuilder.append("UTCTime = ").append(sdf.format(date)).append(AbstractFormWriter.LINE_BREAK);

        requestBuilder.append(AbstractFormWriter.LINE_BREAK);
        requestBuilder.append(transfer.getPassword() == null ? "" : transfer.getPassword()).append(AbstractFormWriter.LINE_BREAK);

        for (TransferBid bid : transfer.getBids()) {
            if (bid==null) 
                continue;
            String transferBid = transferBidToString(bid);
            requestBuilder.append(transferBid).append(AbstractFormWriter.LINE_BREAK);
        }
        String limits = transferLimitsToString(transfer);
        requestBuilder.append(limits).append(AbstractFormWriter.LINE_BREAK);
        requestBuilder.append("-987654/").append(AbstractFormWriter.LINE_BREAK);

        return requestBuilder.toString();
    }

    private static String transferLimitsToString(Transfer transfer) {
        StringBuilder sb = new StringBuilder();
        sb.append(transfer.getMaxGK());
        sb.append('/');
        sb.append(transfer.getMaxLD());
        sb.append('/');
        sb.append(transfer.getMaxCD());
        sb.append('/');
        sb.append(transfer.getMaxRD());
        sb.append('/');
        sb.append(transfer.getMaxLM());
        sb.append('/');
        sb.append(transfer.getMaxCM());
        sb.append('/');
        sb.append(transfer.getMaxRM());
        sb.append('/');
        sb.append(transfer.getMaxLF());
        sb.append('/');
        sb.append(transfer.getMaxCF());
        sb.append('/');
        sb.append(transfer.getMaxRF());
        sb.append('/');
        sb.append(transfer.getMaxPlayers());
        sb.append('/');
        sb.append(transfer.getMinCash());
        sb.append('/');
        return sb.toString();
    }

    public static String transferBidToString(TransferBid bid) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(bid.getId());
        sb.append('/');
        sb.append(bid.getName());
        sb.append('/');
        sb.append(bid.getPreviousTeam());
        sb.append('/');
        sb.append(bid.getPrice());
        sb.append('/');
        sb.append(bid.getTradeIn());
        sb.append('/');
        return sb.toString();
    }
    
    protected static String[] getAdditionalDefenceValues(Transfer form) {
        return new String[] {
            form.getTeamID()
        };
    }
}
