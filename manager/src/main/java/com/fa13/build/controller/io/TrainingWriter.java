package com.fa13.build.controller.io;

import com.fa13.build.model.training.PlayerTraining;
import com.fa13.build.model.training.TrainingForm;

public class TrainingWriter extends AbstractFormWriter<TrainingForm> {

    @Override
    protected void appendFormData(StringBuilder contentBuilder, TrainingForm form) {
        appendLine(contentBuilder, form.getPassword() == null ? "" : form.getPassword());
        appendLine(contentBuilder, form.isScouting() ? form.getMinTalent() : 0);
        for (PlayerTraining playerTraining : form.getPlayers()) {
            appendLine(contentBuilder, createPlayerTrainingLine(playerTraining));
        }
        appendLine(contentBuilder, "999/");
    }

    @Override
    protected String[] getAdditionalDefenceValues(TrainingForm form) {
        return new String[] {
            form.getTeamID()
        };
    }

    private String createPlayerTrainingLine(PlayerTraining training) {
        StringBuilder sb = new StringBuilder();

        sb.append(training.getNumber());
        sb.append('/');
        
        appendPoints(sb, training.getShootingPoints());
        appendPoints(sb, training.getPassingPoints());
        appendPoints(sb, training.getCrossPoints());
        appendPoints(sb, training.getDribblingPoints());
        appendPoints(sb, training.getTacklingPoints());
        appendPoints(sb, training.getSpeedPoints());
        appendPoints(sb, training.getStaminaPoints());
        appendPoints(sb, training.getHeadingPoints());
        appendPoints(sb, training.getReflexesPoints());
        appendPoints(sb, training.getHandlingPoints());
        appendPoints(sb, training.getFitnessPoints());
        appendPoints(sb, training.getMoraleFinance());
        appendPoints(sb, training.getFitnessFinance());
        return sb.toString();
    }

    private StringBuilder appendPoints(StringBuilder sb, int points) {
        if (points > 0) {
            sb.append(points);
        }
        sb.append('/');
        return sb;
    }
}
