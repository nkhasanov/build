package com.fa13.build.view.game;


import com.fa13.build.utils.ResourceUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class SchemeFieldPanel extends JPanel implements MouseMotionListener {
    private static final String FIELD_IMAGE_LOCATION = "/com/fa13/build/resources/images/field.jpg";
    private static final String PLAYER_IMAGE_LOCATION = "/com/fa13/build/resources/images/player.png";
    private static final String PLAYER_SELECTED_IMAGE_LOCATION = "/com/fa13/build/resources/images/player_hl.png";

    private BufferedImage fieldImage;
    private ImageIcon playerIcon;

    private JLayeredPane layeredPane;
    private JComponent player12;

    public SchemeFieldPanel() {
        loadImages();

        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

        Dimension size = new Dimension(770, 533);
        setPreferredSize(size);
        setMinimumSize(size);
        setMaximumSize(size);

        layeredPane = new JLayeredPane();
        layeredPane.setPreferredSize(size);
        layeredPane.addMouseMotionListener(this);

        player12 = createPlayer (100, 100);
        createPlayer (200, 100);
        createPlayer (100, 200);

        add(layeredPane);
    }

    protected void loadImages() {
        try {
            fieldImage = ResourceUtils.getClasspathResourceAsImage(FIELD_IMAGE_LOCATION);
            playerIcon = ResourceUtils.getClasspathResourceAsImageIcon(PLAYER_IMAGE_LOCATION);
        } catch (IOException e) {
            throw new RuntimeException("Cannot initialize SchemeFieldPanel", e);
        }
    }

    protected JComponent createPlayer(int initialX, int initialY) {
//        JLayeredPane playerComponent = new JLayeredPane();
//        playerComponent.setPreferredSize(new Dimension(playerIcon.getIconWidth(), playerIcon.getIconHeight()));
//        playerComponent.setBackground(Color.BLACK);

        JLabel playerIconComponent = new JLabel(playerIcon);
        playerIconComponent.setBounds(initialX, initialY, playerIcon.getIconWidth(), playerIcon.getIconHeight());
//        playerIconComponent.setOpaque(true);

        JLabel playerNumberComponent = new JLabel("12", SwingConstants.CENTER);
        playerNumberComponent.setBounds(initialX, initialY, playerIcon.getIconWidth(), playerIcon.getIconHeight());
//        playerNumberComponent.setOpaque(true);

//        playerComponent.add(playerIconComponent, 1, 0);
//        playerComponent.add(playerNumberComponent, 2, 0);
//        layeredPane.add(playerComponent, 1, 0);
        layeredPane.add(playerIconComponent, 1, 0);
        layeredPane.add(playerNumberComponent, 2, 0);

        return playerIconComponent;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(fieldImage, 0, 0, null);
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        player12.setLocation(e.getX(), e.getY());
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }
}