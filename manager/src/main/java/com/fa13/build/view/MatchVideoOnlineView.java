package com.fa13.build.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.SWTError;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.graphics.ImageData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.fa13.build.api.IDisposable;
import com.fa13.build.api.ILoadable;
import com.fa13.build.api.IStorable;
import com.fa13.build.controller.io.SchemeWriter;
import com.fa13.build.model.Competitions;
import com.fa13.build.model.Competitions.CompetitionStage;
import com.fa13.build.model.MatchInfo;
import com.fa13.build.model.Teams;
import com.fa13.build.utils.GuiLongTask;
import com.fa13.build.utils.GuiUtils;
import com.fa13.build.utils.HttpUtils;
import com.fa13.build.view.MainWindow.ColorResourceType;
import com.fa13.build.view.MainWindow.ImageType;

public class MatchVideoOnlineView implements UIItem, IDisposable, IStorable, ILoadable {

    private Display display;
    private TabFolder parentInstance;
    private TabItem mainItem;
    private Composite topPanel;
    private Composite bottomPanel;
    private Composite mainComposite;
    private Browser browser;
    private Combo comboCompetition;
    private Combo comboCompetitionStage;
    private Combo comboCompetitonRound;
    private Label labelCompetition;
    private Label labelCompetitionStage;
    private Label labelCompetitionRound;
    private Table matchesTable;
    private Composite matchesPanel;
    private Button videoButton;
    private Button reportButton;
    private Button buttonHideShow;
    private Image imageVideo;
    private Image imageReport;
    private String currentCompetitionCode;
    private CompetitionStage currentCompetitionStage;
    private Font matchesTableFont;
    private Font chooserFont;
    private String[] matchesTableTitles;
    private FormData topPanelFormData;
    private FormData bottomPanelFormData;

    public static final String LOCAL_VIDEO_MATCHES_FOLDER = "video";
    public static final String LOCAL_VIDEO_MATCHES_REESTR = "videoMatchesReestr.txt";
    public static final String PROP_VIDEO_CURRENT_COMPETITION_CODE = "video.CurrentCompetitionCode";
    public static final String PROP_VIDEO_CURRENT_COMPETITION_STAGE_NAME = "video.CurrentCompetitionStageName";

    public static Properties localVideoMatchesReestr;
    private boolean filterPanelShown = true;
    private Image imageFilterWindowShow;
    private Image imageFilterWindowHide;
    private Label labelTeamFilter;
    private Text textTeamFilter;

    private String lastTeamFilterText = "";
    private Button applyFilterButton;
    
    public MatchVideoOnlineView(TabFolder parent, Display display) {

        this.display = display;
        this.parentInstance = parent;
        loadImages();
        createGUI();

    }

    private void createGUI() {

        mainComposite = new Composite(parentInstance, SWT.NONE);
        mainComposite.setRedraw(false);
        mainComposite.setLayout(new FormLayout());

        topPanel = new Composite(mainComposite, SWT.BORDER);
        topPanel.setLayout(new GridLayout(5, false));

        Font initialFont = mainComposite.getFont();
        FontData[] fontData = initialFont.getFontData();
        for (int i = 0; i < fontData.length; i++) {
            fontData[i].setHeight(9);
            fontData[i].setStyle(SWT.BOLD);

        }
        chooserFont = new Font(display, fontData);

        buttonHideShow = new Button(topPanel, SWT.PUSH);
        GridData gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        gridData.horizontalSpan = 1;
        buttonHideShow.setLayoutData(gridData);
        buttonHideShow.setImage(imageFilterWindowHide);
        buttonHideShow.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                showHideBrowser();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        labelTeamFilter = new Label(topPanel, SWT.NONE);
        labelTeamFilter.setText(MainWindow.getMessage("global.team"));
        gridData = new GridData(SWT.LEFT, SWT.CENTER, false, false);
        labelTeamFilter.setLayoutData(gridData);
        labelTeamFilter.setFont(chooserFont);

        textTeamFilter = new Text(topPanel, SWT.BORDER);
        gridData = new GridData(SWT.FILL, SWT.CENTER, true, false);
        textTeamFilter.setLayoutData(gridData);
        textTeamFilter.setFont(chooserFont);
        textTeamFilter.addModifyListener(new ModifyListener() {

            @Override
            public void modifyText(ModifyEvent e) {

                applyFilterButton.setVisible(!textTeamFilter.getText().equalsIgnoreCase(lastTeamFilterText));

            }
        });

        applyFilterButton = new Button(topPanel, SWT.PUSH);
        applyFilterButton.setImage(MainWindow.getSharedImage(ImageType.OK));
        applyFilterButton.addSelectionListener(new SelectionListener() {
            
            @Override
            public void widgetSelected(SelectionEvent e) {
                lastTeamFilterText = textTeamFilter.getText();
                loadMatches();
                applyFilterButton.setVisible(false);
            }
            
            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        } );
        applyFilterButton.setVisible(false);
        
        matchesPanel = new Composite(topPanel, SWT.BORDER);
        matchesPanel.setLayout(new GridLayout(2, false));
        gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
        gridData.verticalSpan = 4;
        matchesPanel.setLayoutData(gridData);

        labelCompetition = new Label(topPanel, SWT.NONE);
        labelCompetition.setText(MainWindow.getMessage("competition") + " (*)");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, false, false);
        labelCompetition.setLayoutData(gridData);
        labelCompetition.setFont(chooserFont);

        comboCompetition = new Combo(topPanel, SWT.DROP_DOWN | SWT.READ_ONLY);
        comboCompetition.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        gridData.horizontalSpan = 3;
        comboCompetition.setLayoutData(gridData);
        comboCompetition.setFont(chooserFont);

        labelCompetitionStage = new Label(topPanel, SWT.NONE);
        labelCompetitionStage.setText(MainWindow.getMessage("competitionStage") + " (*)");
        gridData = new GridData(SWT.LEFT, SWT.CENTER, false, false);
        labelCompetitionStage.setLayoutData(gridData);
        labelCompetitionStage.setFont(chooserFont);

        comboCompetitionStage = new Combo(topPanel, SWT.DROP_DOWN | SWT.READ_ONLY);
        comboCompetitionStage.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        gridData.horizontalSpan = 3;
        comboCompetitionStage.setLayoutData(gridData);
        comboCompetitionStage.setFont(chooserFont);

        labelCompetitionRound = new Label(topPanel, SWT.NONE);
        labelCompetitionRound.setText(MainWindow.getMessage("competitionRound"));
        gridData = new GridData(SWT.LEFT, SWT.CENTER, false, false);
        labelCompetitionRound.setLayoutData(gridData);
        labelCompetitionRound.setFont(chooserFont);

        comboCompetitonRound = new Combo(topPanel, SWT.DROP_DOWN | SWT.READ_ONLY);
        comboCompetitonRound.setBackground(MainWindow.getSharedColorResource(ColorResourceType.YELLOW));
        gridData = new GridData(SWT.LEFT, SWT.CENTER, true, false);
        gridData.horizontalSpan = 3;
        comboCompetitonRound.setLayoutData(gridData);
        comboCompetitonRound.setFont(chooserFont);
        //but now it is unused
        comboCompetitonRound.setVisible(false);
        labelCompetitionRound.setVisible(false);

        matchesTable = new Table(matchesPanel, SWT.FULL_SELECTION | SWT.SINGLE);
        matchesTable.setHeaderVisible(true);
        matchesTable.setLinesVisible(true);
        matchesTable.setHeaderVisible(true);

        initialFont = matchesTable.getFont();
        fontData = initialFont.getFontData();
        for (int i = 0; i < fontData.length; i++) {
            fontData[i].setHeight(9);
            fontData[i].setStyle(SWT.BOLD);

        }
        matchesTableFont = new Font(display, fontData);
        matchesTable.setFont(matchesTableFont);

        String[] titles = { "#", MainWindow.getMessage("competitionRound"), MainWindow.getMessage("homeTeam"), " ", MainWindow.getMessage("awayTeam") };
        matchesTableTitles = titles;

        for (int i = 0; i < matchesTableTitles.length; i++) {
            TableColumn column = new TableColumn(matchesTable, SWT.NONE);
            column.setText(matchesTableTitles[i]);
            column.pack();
            column.setResizable(false);
        }
        gridData = new GridData(SWT.FILL, SWT.FILL, true, true);
        gridData.verticalSpan = 2;
        matchesTable.setLayoutData(gridData);

        videoButton = new Button(matchesPanel, SWT.PUSH);
        videoButton.setImage(imageVideo);
        videoButton.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                int index = matchesTable.getSelectionIndex();
                if (index != -1) {
                    TableItem tableItem = matchesTable.getItem(index);
                    MatchInfo matchInfo = (MatchInfo) tableItem.getData();
                    if (matchInfo != null) {
                        if (matchInfo.getVideoUrl() != null) {
                            //System.out.println("open browser with url: " + videoPlayerUrl);
                            String videoPlayerUrl = "http://www.fa13.info/video/match.php?video=";
                            videoPlayerUrl = videoPlayerUrl + matchInfo.getVideoUrl().substring(6);
                            browser.setUrl(videoPlayerUrl);
                        } else {
                            //System.out.println("video url is null: " + matchInfo.getHomeTeamCode() + "-" + matchInfo.getAwayTeamCode());
                        }

                    }
                }
                filterPanelShown = true;
                showHideBrowser();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });
        gridData = new GridData(SWT.CENTER, SWT.TOP, false, false);
        videoButton.setLayoutData(gridData);

        reportButton = new Button(matchesPanel, SWT.PUSH);
        reportButton.setImage(imageReport);
        reportButton.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                int index = matchesTable.getSelectionIndex();
                if (index != -1) {
                    TableItem tableItem = matchesTable.getItem(index);
                    MatchInfo matchInfo = (MatchInfo) tableItem.getData();
                    if (matchInfo != null) {
                        browser.setUrl("http://www.fa13.info/" + matchInfo.getReportUrl().substring(6));
                    }
                }
                filterPanelShown = true;
                showHideBrowser();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });
        gridData = new GridData(SWT.CENTER, SWT.TOP, false, false);
        reportButton.setLayoutData(gridData);

        FormData formData = new FormData();
        formData.top = new FormAttachment(0, 0);
        formData.bottom = new FormAttachment(0, topPanel.computeSize(SWT.DEFAULT, SWT.DEFAULT).y);
        formData.left = new FormAttachment(0, 0);
        formData.right = new FormAttachment(100, 0);
        formData.height = 50;
        topPanel.setLayoutData(formData);
        topPanelFormData = formData;

        bottomPanel = new Composite(mainComposite, SWT.BORDER);
        bottomPanel.setLayout(new FormLayout());
        formData = new FormData();
        formData.top = new FormAttachment(topPanel, 0);
        formData.bottom = new FormAttachment(100, 0);
        formData.left = new FormAttachment(0, 0);
        formData.right = new FormAttachment(100, 0);
        bottomPanel.setLayoutData(formData);
        bottomPanelFormData = formData;

        comboCompetition.select(-1);
        comboCompetition.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                int selected = comboCompetition.getSelectionIndex();
                currentCompetitionCode = null;
                if (selected != -1) {
                    currentCompetitionCode = comboCompetition.getItem(selected);
                    currentCompetitionCode = comboCompetition.getData(currentCompetitionCode).toString();
                }
                setupCompetitionStage(currentCompetitionCode);
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        comboCompetitionStage.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                loadMatches();
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        //http://www.fa13.info/video/match.php?video=pub/graf/cGe8/Sht-V05.zip
        //http://www.fa13.info/video/match.php?video=pub/graf/cCL0/Kar-P27.zip
        //http://www.fa13.info/WC.html
        //http://www.fa13.info/video/match.php?video=pub/graf/rGe30/Fra-Krl.zip
        //browser.setUrl("http://www.fa13.info/WC.html");
        /*        
                browser.addLocationListener(new LocationListener() {
                    
                    @Override
                    public void changing(LocationEvent event) {
                        // Call user code to process link as desired and return
                        // true if the link should be opened in place.
                        // Setting event.doit to false prevents the link from opening in place
                        event.doit = true;
                        currentUrl = event.location;
                        System.out.println(event.location);
                    }
                    
                    @Override
                    public void changed(LocationEvent event) {
                        // TODO Auto-generated method stub
                        
                    }
                });
                browser.addOpenWindowListener(new OpenWindowListener() {
                    
                    @Override
                    public void open(WindowEvent event) {
                        
                        //browser.setUrl(currentUrl);
                        
                    }
                });
        */
        try {
            browser = new Browser(bottomPanel, SWT.BORDER);
            formData = new FormData();
            formData.top = new FormAttachment(0, 0);
            formData.bottom = new FormAttachment(100, 0);
            formData.left = new FormAttachment(0, 0);
            formData.right = new FormAttachment(100, 0);
            browser.setLayoutData(formData);
        } catch (SWTError e) {
            GuiUtils.showErrorMessage(parentInstance.getShell(), MainWindow.getMessage("error.cannotCreateBrowser.swtError.title"),
                    MainWindow.getMessage("error.cannotCreateBrowser.swtError.details"));
        } catch (Exception e) {
            GuiUtils.showErrorMessage(parentInstance.getShell(), MainWindow.getMessage("error.cannotCreateBrowser.generic.title"),
                    MainWindow.getMessage("error.cannotCreateBrowser.generic.details") + "\n" + e.getLocalizedMessage());
        }
        mainItem = new TabItem(parentInstance, SWT.NONE);
        mainItem.setText(MainWindow.getMessage("video.online"));
        mainItem.setControl(mainComposite);
        mainComposite.setRedraw(true);
    }

    //private String currentUrl;

    private void showHideBrowser() {
        filterPanelShown = !filterPanelShown;
        if (filterPanelShown) {
            buttonHideShow.setToolTipText(MainWindow.getMessage("hideFilter"));
            buttonHideShow.setImage(imageFilterWindowHide);
            int maxY = mainComposite.computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
            int y = topPanel.computeSize(SWT.DEFAULT, SWT.DEFAULT).y;
            if (y > maxY) {
                y = maxY;
            }
            topPanelFormData.bottom.offset = y;
            mainComposite.layout();
        } else {
            buttonHideShow.setToolTipText(MainWindow.getMessage("showFilter"));
            buttonHideShow.setImage(imageFilterWindowShow);
            topPanelFormData.bottom.offset = buttonHideShow.computeSize(SWT.DEFAULT, SWT.DEFAULT).y + 5;
            mainComposite.layout();
        }
    }

    private void initLocalVideoMatchesReestr() {

        if (localVideoMatchesReestr == null) {
            localVideoMatchesReestr = new Properties();
        }
        try {
            String fileName = LOCAL_VIDEO_MATCHES_FOLDER + File.separatorChar + LOCAL_VIDEO_MATCHES_REESTR;
            File file = new File(fileName);
            if (file.exists()) {

                localVideoMatchesReestr.loadFromXML(new FileInputStream(file));
                //System.out.println(fileName + " loaded");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveLocalVideoMatchesReestr() {

        if (localVideoMatchesReestr == null) {
            return;
        }
        try {
            String fileName = LOCAL_VIDEO_MATCHES_FOLDER + File.separatorChar + LOCAL_VIDEO_MATCHES_REESTR;

            File file = new File(fileName);
            file.getParentFile().mkdirs();
            file.createNewFile();
            localVideoMatchesReestr.storeToXML(new FileOutputStream(fileName), "Fa13 Jogador local video matches file reestr", Charsets.UTF_8.name());
            //System.out.println(fileName + " saved");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadImages() {
        ImageData imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/video_camera.png"));
        imageVideo = new Image(display, imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/report.png"));
        imageReport = new Image(display, imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/window_find_16x16.png"));
        imageFilterWindowShow = new Image(parentInstance.getDisplay(), imgData);

        imgData = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/window_remove_16x16.png"));
        imageFilterWindowHide = new Image(parentInstance.getDisplay(), imgData);
    }

    //do not touch this service util !!!
    /*
            private StringBuilder getAllTeamsAsProperties() {
                StringBuilder sb = new StringBuilder();
                if (MainWindow.getAllInstance() != null && MainWindow.getAllInstance().getTeams() != null) {
                    for (Team team : MainWindow.getAllInstance().getTeams()) {
                        sb.append(team.getName()).append("=").append(team.getId()).append(GuiUtils.LINE_BREAK);
                    }
                }
                return sb;
            }
    */

    public void setupMatchChooser() {
        /*
        if (MainWindow.getAllInstance() != null && MainWindow.getAllInstance().getCompetitions() != null
                && MainWindow.getAllInstance().getCompetitions().entrySet() != null) {

            Set<Entry<String, String>> entrySet = MainWindow.getAllInstance().getCompetitions().entrySet();
            comboCompetition.removeAll();
            for (Entry<String, String> entry : entrySet) {
                comboCompetition.add(entry.getValue());
                comboCompetition.setData(entry.getValue(), entry.getKey());
            }

        }
        */
        //add absent competitions from default competitions
        for (Entry<String, String> entry : Competitions.getDefaultCompetitions().entrySet()) {
            //int index = GuiUtils.getComboItemIndex(comboCompetition, entry.getKey());
            //if (index == -1) {
            if (!Competitions.isGroupCompetition(entry.getValue()) && !entry.getValue().equals(Competitions.WORLD_FINAL_CODE)
                    && !entry.getValue().equals(Competitions.WORLD_U21_FINAL_CODE)) {
                comboCompetition.add(entry.getKey());
                comboCompetition.setData(entry.getKey(), entry.getValue());
            }
            //}
        }

        comboCompetition.computeSize(SWT.DEFAULT, SWT.DEFAULT, true);
        comboCompetition.getParent().layout();

        //setupCompetitionStage(null);
        setupCompetitionRounds();
    }

    private void setupCompetitionRounds() {
        comboCompetitonRound.removeAll();
        for (int i = 0; i < Competitions.ROUND_TYPES.length; i++) {
            comboCompetitonRound.add(Competitions.ROUND_TYPES[i]);
            comboCompetitonRound.setData(Competitions.ROUND_TYPES[i], Competitions.ROUND_TYPE_NUMBER.get(Competitions.ROUND_TYPES[i]));
        }
    }

    private void setupCompetitionStage(String competitionCode) {

        matchesTable.removeAll();
        comboCompetitionStage.removeAll();

        if (competitionCode == null) {
            return;
        }
        //comboCompetitionStage.select(-1);
        comboCompetitionStage.add(Competitions.CompetitionStage.LAST_GAMES.toString());
        comboCompetitionStage.add(Competitions.CompetitionStage.ALL_GAMES.toString());
        comboCompetitionStage.setData(Competitions.CompetitionStage.LAST_GAMES.toString(), Competitions.CompetitionStage.LAST_GAMES);
        comboCompetitionStage.setData(Competitions.CompetitionStage.ALL_GAMES.toString(), Competitions.CompetitionStage.ALL_GAMES);

        if (competitionCode.equals(Competitions.CHAMPIONS_LEAGUE_CODE) || competitionCode.equals(Competitions.ASSOCIATION_CUP_CODE)
                || competitionCode.equals(Competitions.FEDERATION_CUP_CODE)) {

            comboCompetitionStage.add(Competitions.CompetitionStage.QUALIFICATION.toString());
            comboCompetitionStage.setData(Competitions.CompetitionStage.QUALIFICATION.toString(), Competitions.CompetitionStage.QUALIFICATION);
            comboCompetitionStage.add(Competitions.CompetitionStage.FINAL.toString());
            comboCompetitionStage.setData(Competitions.CompetitionStage.FINAL.toString(), Competitions.CompetitionStage.FINAL);
        }
        if (competitionCode.equals(Competitions.ASSOCIATION_CUP_CODE)) {
            comboCompetitionStage.add(Competitions.CompetitionStage.PREPHASE_ROUND.toString());
            comboCompetitionStage.setData(Competitions.CompetitionStage.PREPHASE_ROUND.toString(), Competitions.CompetitionStage.PREPHASE_ROUND);
        }

        if (competitionCode.equals(Competitions.WORLD_CODE)) {
            comboCompetitionStage.add(Competitions.CompetitionStage.PREPHASE_GROUP.toString());
            comboCompetitionStage.setData(Competitions.CompetitionStage.PREPHASE_GROUP.toString(), Competitions.CompetitionStage.PREPHASE_GROUP);

        }

        if (competitionCode.equals(Competitions.WORLD_CODE) || competitionCode.equals(Competitions.WORLD_U21_CODE)) {
            comboCompetitionStage.add(Competitions.CompetitionStage.FINAL.toString());
            comboCompetitionStage.setData(Competitions.CompetitionStage.FINAL.toString(), Competitions.CompetitionStage.FINAL);
        }

        comboCompetitionStage.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        comboCompetitionStage.getParent().layout();
        comboCompetitionStage.select(-1);
        /*        
                comboCompetitionStage.add(Competitions.CompetitionStage.PREPHASE_ROUND.toString());
                //comboCompetitionStage.add(Competitions.CompetitionStage.PREPHASE_GROUP.toString());
                comboCompetitionStage.add(Competitions.CompetitionStage.QUALIFICATION.toString());
                //comboCompetitionStage.add(Competitions.CompetitionStage.GROUP.toString());
                comboCompetitionStage.add(Competitions.CompetitionStage.FINAL.toString());
                //comboCompetitionStage.add(Competitions.CompetitionStage.TRANSITIONAL.toString());

                comboCompetitionStage.setData(Competitions.CompetitionStage.PREPHASE_ROUND.toString(), Competitions.CompetitionStage.PREPHASE_ROUND);
        //        comboCompetitionStage.setData(Competitions.CompetitionStage.PREPHASE_GROUP.toString(), Competitions.CompetitionStage.PREPHASE_GROUP);
                comboCompetitionStage.setData(Competitions.CompetitionStage.QUALIFICATION.toString(), Competitions.CompetitionStage.QUALIFICATION);
        //        comboCompetitionStage.setData(Competitions.CompetitionStage.GROUP.toString(), Competitions.CompetitionStage.GROUP);
                comboCompetitionStage.setData(Competitions.CompetitionStage.FINAL.toString(), Competitions.CompetitionStage.FINAL);
        //        comboCompetitionStage.setData(Competitions.CompetitionStage.TRANSITIONAL.toString(), Competitions.CompetitionStage.TRANSITIONAL);
         * 
         */
    }

    public class MatchInfoComparator implements Comparator<MatchInfo> {
        private boolean ascending;

        public MatchInfoComparator(boolean isAscending) {
            ascending = isAscending;
        }

        @Override
        public int compare(MatchInfo m1, MatchInfo m2) {
            try {
                int o1 = Integer.valueOf(m1.getCompetitionRound());
                int o2 = Integer.valueOf(m2.getCompetitionRound());

                if (m1.getCompetitionCode().charAt(0) == m2.getCompetitionCode().charAt(0)) {
                    if (ascending) {
                        if (m1.getCompetitionCode().charAt(0) == 'c') {
                            return (o1 > o2 ? -1 : (o1 == o2 ? 0 : 1));
                        } else {
                            return (o2 > o1 ? -1 : (o1 == o2 ? 0 : 1));
                        }
                    } else {
                        if (m1.getCompetitionCode().charAt(0) == 'c') {
                            return (o2 > o1 ? -1 : (o1 == o2 ? 0 : 1));
                        } else {
                            return (o1 > o2 ? -1 : (o1 == o2 ? 0 : 1));
                        }
                    }

                } else {
                    // r & c 
                    if (ascending) {
                        return m1.getCompetitionCode().charAt(0) == 'c' ? 1 : -1;
                    } else {
                        return m2.getCompetitionCode().charAt(0) == 'c' ? 1 : -1;
                    }
                }

            } catch (Exception e) {
                return 0;
            }

        }
    }

    private void loadMatches() {
        //System.out.println("loadMatches...");
        GuiLongTask task = new GuiLongTask() {

            @Override
            protected void execute() {

                int selected = comboCompetition.getSelectionIndex();
                String compCode = null;
                if (selected != -1) {
                    compCode = comboCompetition.getItem(selected);
                    compCode = comboCompetition.getData(compCode).toString();
                }

                selected = comboCompetitionStage.getSelectionIndex();
                currentCompetitionStage = null;
                if (selected != -1) {
                    String compStageKey = comboCompetitionStage.getItem(selected);
                    currentCompetitionStage = (CompetitionStage) comboCompetitionStage.getData(compStageKey);
                }

                String urlString = Competitions.getCompetitionUrl(compCode, currentCompetitionStage);
                List<MatchInfo> matches = filterMatches(processMatches(urlString));
                if (matches != null) {
                    Collections.sort(matches, new MatchInfoComparator(false));
                }
                matchesTable.removeAll();
                if (matches != null) {
                    matchesTable.setItemCount(matches.size());
                    String name;
                    Color color = display.getSystemColor(SWT.COLOR_YELLOW);
                    for (int i = 0; i < matches.size(); i++) {
                        TableItem tableItem = matchesTable.getItem(i);
                        MatchInfo matchInfo = matches.get(i);
                        int numRound = Integer.valueOf(matchInfo.getCompetitionRound());
                        String compStageName = Competitions.getMatchRoundName(matchInfo.getCompetitionCode(), numRound);
                        tableItem.setText(0, String.valueOf(i + 1));
                        tableItem.setText(1, compStageName);
                        name = StringUtils.defaultString(matchInfo.getHomeTeamName(), MainWindow.getMessage("underfined"));
                        tableItem.setText(2, name);
                        tableItem.setText(3, " - ");
                        name = StringUtils.defaultString(matchInfo.getAwayTeamName(), MainWindow.getMessage("underfined"));
                        tableItem.setText(4, name);
                        tableItem.setData(matchInfo);
                        tableItem.setBackground(color);
                    }
                    for (int i = 0; i < matchesTable.getColumnCount(); i++) {
                        matchesTable.getColumn(i).pack();
                    }
                    matchesTable.computeSize(SWT.DEFAULT, SWT.DEFAULT);
                    matchesTable.getParent().layout();
                }
            };
        };

        GuiUtils.showModalProgressWindow(parentInstance.getShell(), task, MainWindow.getMessage("matchesListBuild"), false);
    }

    enum MatchesProcessStatus {
        BEGIN, OK, DOWNLOAD_ERROR, ERROR
    }

    private MatchesProcessStatus matchesProcessStatus;

    public static String LOCAL_VIDEO_FILE_EXSTENSION = ".txt";

    public List<MatchInfo> filterMatches(List<MatchInfo> matchesList) {
        if (matchesList == null) {
            return null;
        }

        String filterText = textTeamFilter.getText();

        if (filterText != null) {
            filterText = filterText.toUpperCase().trim();
        }

        if (filterText == null || filterText.isEmpty()) {
            return matchesList;
        }

        List<MatchInfo> filteredList = new ArrayList<MatchInfo>();

        for (MatchInfo matchInfo : matchesList) {
            if (matchInfo.getHomeTeamName().toUpperCase().contains(filterText)) {
                filteredList.add(matchInfo);
            } else if (matchInfo.getAwayTeamName().toUpperCase().contains(filterText)) {
                filteredList.add(matchInfo);
            }

        }

        return filteredList;
    }

    public List<MatchInfo> processMatches(final String competitionUrl) {
        matchesProcessStatus = MatchesProcessStatus.BEGIN;

        //System.out.println("processMatches(" + competitionUrl + ")");
        List<MatchInfo> matchesList = null;
        
        if (competitionUrl == null) {
            matchesProcessStatus = MatchesProcessStatus.ERROR;
            return null;
        }
        
        long localModified = -1;
        Object localVideoFileKey = localVideoMatchesReestr.get(competitionUrl);
        String localFileName = (localVideoFileKey == null ? null : localVideoFileKey.toString());
        String localFileNameBeforeModify = localFileName;
        if (localVideoMatchesReestr != null) {

            if (localFileName != null) {
                //relative path
                localFileName = LOCAL_VIDEO_MATCHES_FOLDER + File.separatorChar + localFileName + LOCAL_VIDEO_FILE_EXSTENSION;

                File file = new File(localFileName);
                if (file.exists()) {
                    //read first line with last loaded url modified stamp
                    BufferedReader reader = null;
                    try {

                        reader = new BufferedReader(new InputStreamReader(new FileInputStream(localFileName), SchemeWriter.UTF8_CHARSET));

                        String firstLine = reader.readLine();
                        localModified = Long.valueOf(firstLine);
                        reader.close();

                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            reader.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }
        //get url modified time and check if we need to download/parse matches
        URL url;
        try {
            url = new URL(competitionUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.connect();
            long lastModified = conn.getLastModified();
            //System.out.println("got competition url lastModified = " + lastModified);
            //System.out.println(conn.getHeaderFields());
            //System.out.println(conn.getContentEncoding());
            //System.out.println(conn.getContentType());
            //Map<String, List<String>> headerFields = conn.getHeaderFields();
            //if lastModified=0 then server not set it so we are to download it again and again!!!
            if (lastModified == 0 || lastModified - localModified > 0) {
                //need to download url again/first time
                String urlContent = HttpUtils.getRemoteContent(competitionUrl);
                if (StringUtils.isEmpty(urlContent)) {
                    matchesProcessStatus = MatchesProcessStatus.DOWNLOAD_ERROR;
                    GuiUtils.showErrorMessage(parentInstance.getShell(), MainWindow.getMessage("error"),
                            MainWindow.getMessage("matchesListDownloadFailed"));
                }
                //urlContents.insert(0, String.valueOf(lastModified) + GuiUtils.LINE_BREAK);
                //save to new file
                //find max free available number in video reestr
                int maxNumber = 0;
                for (Object valObj : localVideoMatchesReestr.values()) {
                    try {
                        int val = Integer.valueOf(valObj.toString());
                        if (val > maxNumber) {
                            maxNumber = val;
                        }
                    } catch (Exception e) {
                    }

                }
                maxNumber++;

                matchesList = extractMatches(urlContent);
                if (localFileNameBeforeModify == null) {
                    localFileName = String.valueOf(maxNumber);
                    localVideoMatchesReestr.put(competitionUrl, localFileName);
                    localFileName = localFileName + LOCAL_VIDEO_FILE_EXSTENSION;
                } else {
                    //restore original
                    localFileName = localFileNameBeforeModify + LOCAL_VIDEO_FILE_EXSTENSION;
                }
                File fileToSave = new File(LOCAL_VIDEO_MATCHES_FOLDER + File.separatorChar + localFileName);
                if (!fileToSave.getParentFile().exists()) {
                    fileToSave.getParentFile().mkdirs();
                }
                fileToSave.createNewFile();
                String contents = matchesListAsStringBuilder(matchesList).insert(0, String.valueOf(lastModified) + GuiUtils.LINE_BREAK).toString();
                FileUtils.writeStringToFile(fileToSave, contents, Charsets.UTF_8);
            } else {
                //already have up to date local matches list
                //load them
                File file = new File(localFileName);
                String contents = FileUtils.readFileToString(file, Charsets.UTF_8);
                matchesList = parseMatchesList(contents);
            }
            //so file is created
            matchesProcessStatus = MatchesProcessStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return matchesList;
    }

    private static final String MATCH_DATA_SEPARATOR = ";";

    //format is:

    //modified
    //homeTeamCode;homeTeamName;awayTeamCode;awayTeamName
    //reportUrl1
    //videoUrl1
    //.... .... ... ... 
    //homeTeamCode;homeTeamName;awayTeamCode;awayTeamName
    //reportUrlN
    //videoUrlN
    private StringBuilder matchesListAsStringBuilder(List<MatchInfo> matchesList) {
        StringBuilder sb = new StringBuilder();
        for (MatchInfo matchInfo : matchesList) {
            sb.append(matchInfo.getHomeTeamCode()).append(MATCH_DATA_SEPARATOR).append(matchInfo.getHomeTeamName());
            sb.append(MATCH_DATA_SEPARATOR);
            sb.append(matchInfo.getAwayTeamCode()).append(MATCH_DATA_SEPARATOR).append(matchInfo.getAwayTeamName());
            sb.append(GuiUtils.LINE_BREAK);
            sb.append(matchInfo.getReportUrl()).append(GuiUtils.LINE_BREAK);
            sb.append(matchInfo.getVideoUrl()).append(GuiUtils.LINE_BREAK);
        }
        return sb;
    }

    private List<MatchInfo> parseMatchesList(String s) {
        List<MatchInfo> matchesList = new ArrayList<MatchInfo>();
        String ss[] = s.split(GuiUtils.LINE_BREAK);
        return matchesList;
    }

    public static final Pattern PATTERN_MATCH_VIDEO_ZIP = Pattern.compile("(\\.\\./\\.\\./pub/graf/[^\\s/]+/[^\\s/]+\\.zip)");
    public static final Pattern PATTERN_MATCH_REPORT_HTML = Pattern.compile("(\\.\\./\\.\\./pub/html/[^\\s/]+/[^\\s/]+\\.htm)");

    private List<MatchInfo> extractMatches(String htmlContent) {
        List<String> videosList = new ArrayList<String>();
        List<String> reportsList = new ArrayList<String>();
        List<MatchInfo> matchesList = new ArrayList<MatchInfo>();

        Matcher m = PATTERN_MATCH_REPORT_HTML.matcher(htmlContent);

        while (m.find()) {
            String s = m.group();
            //System.out.println("found report: " + s);
            //System.out.println("[" + m.start() + ", " + m.end() + "]  len=" + (m.end() - m.start()));
            reportsList.add(s);
            MatchInfo matchInfo = new MatchInfo();
            matchInfo.setReportUrl(s);

            int found = matchesList.indexOf(matchInfo);
            if (found == -1) {
                matchesList.add(matchInfo);
                updateTeamNames(matchInfo, Teams.getDefaultAllTeams());
            } else {
                //System.out.println("will not add dublicate report!!!");
            }
        }

        m = PATTERN_MATCH_VIDEO_ZIP.matcher(htmlContent);
        MatchInfo mi = new MatchInfo();
        while (m.find()) {
            String s = m.group();
            //System.out.println("found video: " + s);
            //System.out.println("[" + m.start() + ", " + m.end() + "]  len=" + (m.end() - m.start()));
            videosList.add(s);
            mi.setVideoUrl(s);//!!!
            int found = matchesList.indexOf(mi);
            if (found == -1) {
                //System.out.println("!!! not found match for: " + s);
            } else {
                matchesList.get(found).setVideoUrl(s);
            }
        }

        //I can't understand what wrong with this patterns (PATTERN_MATCH_VIDEO_ZIP, PATTERN_MATCH_VIDEO_HTML)?
        //They produce many duplicates and sometimes matches not founded.
        //So I've decided to do dirty trick for restoring no found matches videos links here.
        //Welcome to extreme programming world :)))
        for (MatchInfo matchInfo : matchesList) {
            if (matchInfo.getVideoUrl() == null) {
                String calcVideoUrl = "../../pub/graf/" + matchInfo.getCompetitionCodeRound() + "/" + matchInfo.getHomeTeamCode() + "-"
                        + matchInfo.getAwayTeamCode() + ".zip";
                matchInfo.setVideoUrl(calcVideoUrl);
                //System.out.println("restored videoUrl: " + calcVideoUrl);
            }
        }

        return matchesList;
    }

    private static void updateTeamNames(MatchInfo matchInfo, Map<String, String> nameCodeTeamsMap) {
        matchInfo.setHomeTeamName(GuiUtils.getKeyByValue(nameCodeTeamsMap, matchInfo.getHomeTeamCode()));
        matchInfo.setAwayTeamName(GuiUtils.getKeyByValue(nameCodeTeamsMap, matchInfo.getAwayTeamCode()));
    }

    @Override
    public void store(Properties props) {
        saveLocalVideoMatchesReestr();
        props.put(PROP_VIDEO_CURRENT_COMPETITION_CODE, currentCompetitionCode == null ? "" : currentCompetitionCode);
        props.put(PROP_VIDEO_CURRENT_COMPETITION_STAGE_NAME, currentCompetitionStage == null ? "" : currentCompetitionStage.name());

    }

    @Override
    public void dispose() {
        imageReport.dispose();
        imageVideo.dispose();
        imageFilterWindowHide.dispose();
        imageFilterWindowShow.dispose();
        if (browser != null && !browser.isDisposed()) {
            browser.dispose();
        }

        matchesTableFont.dispose();
        chooserFont.dispose();
    }

    private boolean needUpdate = true;

    @Override
    public void updateAll() {
        if (!needUpdate)
            return;
        /* used to get all teams as properties
         //do NOT REMOVE IT !!!
        try {
            File file = new File("allTeams_WORLD_U21.txt");
            FileUtils.writeStringToFile( file, getAllTeamsAsProperties().toString(), Charsets.UTF_8 );
            System.out.println("allTeams_WORLD_U21.txt saved");
            System.out.println(file.getAbsoluteFile());
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        */
        initLocalVideoMatchesReestr();
        setupMatchChooser();

        if (currentCompetitionCode != null && !currentCompetitionCode.isEmpty()) {
            for (int i = 0; i < comboCompetition.getItemCount(); i++) {
                String compName = comboCompetition.getItem(i);
                if (comboCompetition.getData(compName).equals(currentCompetitionCode)) {
                    //found competition!
                    //select it
                    comboCompetition.select(i);
                    setupCompetitionStage(currentCompetitionCode);
                    break;
                }
            }
        }

        if (currentCompetitionStage != null) {
            for (int i = 0; i < comboCompetitionStage.getItemCount(); i++) {
                String compStageName = comboCompetitionStage.getItem(i);
                if (comboCompetitionStage.getData(compStageName).equals(currentCompetitionStage)) {
                    //found competition stage!
                    //select it
                    comboCompetitionStage.select(i);

                    break;
                }
            }
        }
        if (currentCompetitionCode != null && !currentCompetitionCode.isEmpty() && currentCompetitionStage != null) {
            loadMatches();
        }
        needUpdate = false;
        //browser.setUrl("http://www.fa13.info/WC.html");
    }

    @Override
    public void updateAll(boolean lazyUpdate) {
        needUpdate = lazyUpdate;
        if (!lazyUpdate) {
            updateAll();
        }
    }

    @Override
    public TabItem asTabItem() {
        return mainItem;
    }

    @Override
    public void updatePassword(String password) {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateMessages() {
        mainItem.setText(MainWindow.getMessage("video.online"));

        int selected = comboCompetition.getSelectionIndex();
        String compCode = null;
        if (selected != -1) {
            compCode = comboCompetition.getItem(selected);
            compCode = comboCompetition.getData(compCode).toString();
        }
        setupCompetitionStage(compCode);
        
        labelTeamFilter.setText(MainWindow.getMessage("global.team"));
        labelCompetition.setText(MainWindow.getMessage("competition") + " (*)");
        labelCompetitionStage.setText(MainWindow.getMessage("competitionStage") + " (*)");
        labelCompetitionRound.setText(MainWindow.getMessage("competitionRound"));
        labelTeamFilter.pack();
        labelCompetition.pack();
        labelCompetitionStage.pack();
        labelCompetitionRound.pack();

        String[] titles = { "#", MainWindow.getMessage("competitionRound"), MainWindow.getMessage("homeTeam"), " ", MainWindow.getMessage("awayTeam") };
        matchesTableTitles = titles;

        for (int i = 0; i < matchesTableTitles.length; i++) {
            TableColumn column = matchesTable.getColumn(i);
            column.setText(matchesTableTitles[i]);
            column.pack();
            column.setResizable(false);
        }
        matchesTable.layout();
        topPanel.layout();

    }

    @Override
    public void redraw() {
        // TODO Auto-generated method stub

    }

    @Override
    public void load(Properties props) {
        currentCompetitionCode = props.getProperty(PROP_VIDEO_CURRENT_COMPETITION_CODE);
        String compStage = props.getProperty(PROP_VIDEO_CURRENT_COMPETITION_STAGE_NAME);
        if (compStage != null && !compStage.isEmpty()) {
            currentCompetitionStage = CompetitionStage.valueOf(compStage);
        }
    }

}