/*
 * FA13 Manager. Application for managing virtual football team in FA13.
 *
 * Copyright (c) 2012-2013 FA13 (http://www.fa13.info).
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see http://www.gnu.org/licenses/.
 */

package com.fa13.build.view;

import com.fa13.build.api.IDisposable;
import com.fa13.build.api.IStorable;
import com.fa13.build.controller.io.VideoReader;
import com.fa13.build.model.*;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.graphics.*;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.widgets.*;
import org.pushingpixels.trident.Timeline.RepeatBehavior;
import org.pushingpixels.trident.swt.SWTRepaintTimeline;

import java.util.Iterator;
import java.util.Properties;

public class MatchVideoView implements UIItem, IDisposable, IStorable {

    private TabFolder parentInstance;
    private TabItem mainItem;
    private Composite mainComposite;
    private Composite bottomPanel;
    private Composite videoFramePanel;
    private Button openButton;
    private Video videoFormInstance;
    private VideoFrame currentVideoFrame;
    private VideoFrame previousVideoFrame;
    private VideoFrame tempVideoFrame;

    CanvasMovie fieldCanvas;
    private Image fieldImage;
    private Image playerImage;

    int maxWidth = 0;
    int maxHeight = 0;
    private ScrolledComposite fieldScrolledPanel;
    private Button playButton;

    boolean play = false;
    final private Display display;
    private Image ballImage;
    private SWTRepaintTimeline timeline;
    private Image playerImage2;
    private Slider videoSlider;
    private Composite videoPanel;
    private Composite teamsInfoPanel;
    private Text teamsInfoText;
    private Slider videoSpeedSlider;
    private Text matchCommentsText;

    private int fieldChange = Integer.MAX_VALUE;

    private Font timeFont;
    private Color timeColor;
    private Font playerNumberFont;
    private Color playerNumberColorHomeTeam;
    private Color playerNumberColorAwayTeam;

    public MatchVideoView(TabFolder parent, Display display) {
        this.display = display;

        this.parentInstance = parent;

        timeFont = createFont(parent, 11, SWT.BOLD);
        timeColor = new Color(parent.getDisplay(), 0, 0, 0);
        playerNumberFont = createFont(parent, 7, SWT.BOLD);
        playerNumberColorHomeTeam = new Color(parent.getDisplay(), 240, 0, 0);
        playerNumberColorAwayTeam = new Color(parent.getDisplay(), 240, 240, 240);

        mainItem = new TabItem(parent, SWT.NONE);
        mainItem.setText(MainWindow.getMessage("videoTabName"));

        FormLayout formLayout = new FormLayout();
        mainComposite = new Composite(parent, SWT.NONE);
        mainItem.setControl(mainComposite);
        mainComposite.setRedraw(false);
        mainComposite.setLayout(formLayout);

        bottomPanel = new Composite(mainComposite, SWT.BORDER);
        FormData data = new FormData();
        data.bottom = new FormAttachment(100, 0);
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        bottomPanel.setLayoutData(data);
        bottomPanel.setLayout(new FormLayout());

        openButton = new Button(bottomPanel, SWT.PUSH);
        data = new FormData();
        Point size = openButton.computeSize(SWT.DEFAULT, SWT.DEFAULT);
        data.top = new FormAttachment(50, -size.y / 2);
        data.right = new FormAttachment(100, -5);
        openButton.setLayoutData(data);
        openButton.addSelectionListener(new OpenButtonListner());

        videoPanel = new Composite(mainComposite, SWT.NONE);
        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.top = new FormAttachment(0, 0);
        data.bottom = new FormAttachment(100, 0);
        videoPanel.setLayoutData(data);
        videoPanel.setLayout(new FormLayout());

        fieldScrolledPanel = new ScrolledComposite(videoPanel, SWT.V_SCROLL
                | SWT.H_SCROLL);
        fieldScrolledPanel.setExpandHorizontal(false);
        fieldScrolledPanel.setExpandVertical(false);

        videoFramePanel = new Composite(fieldScrolledPanel, SWT.NONE);
        fieldScrolledPanel.setContent(videoFramePanel);

        videoSlider = new Slider(videoPanel, SWT.NONE);
        data = new FormData();
        data.width = 698;
        data.top = new FormAttachment(fieldScrolledPanel, 0);
        videoSlider.setLayoutData(data);
        videoSlider.addSelectionListener(new VideoSliderListener());

        playButton = new Button(videoPanel, SWT.PUSH);
        data = new FormData();
        data.width = 25;
        data.height = 25;
        data.top = new FormAttachment(videoSlider, 0);
        playButton.setLayoutData(data);
        playButton.addSelectionListener(new PlayButtonListner());

        videoSpeedSlider = new Slider(videoPanel, SWT.NONE);
        data = new FormData();
        data.width = 300;
        data.top = new FormAttachment(playButton, 0);
        videoSpeedSlider.setLayoutData(data);
        videoSpeedSlider.addSelectionListener(new VideoSpeedSliderListener());
        videoSpeedSlider.setMinimum(10);
        videoSpeedSlider.setMaximum(40);

        matchCommentsText = new Text(videoPanel, SWT.MULTI | SWT.V_SCROLL);
        data = new FormData();
        data.width = 500;
        data.height = 300;
        data.top = new FormAttachment(videoSpeedSlider, 0);
        matchCommentsText.setLayoutData(data);
        matchCommentsText.setEditable(false);
        matchCommentsText.getHorizontalBar();
        matchCommentsText.setFont(new Font(parent.getDisplay(), "Monospaced",
                10, SWT.NONE));

        videoFramePanel.setBackground(new Color(parent.getDisplay(), 0, 0, 0));
        videoFramePanel.setSize(698, 460);
        videoFramePanel.setLayout(new FormLayout());
        fieldImage = new Image(parent.getDisplay(), this.getClass().getResourceAsStream("/com/fa13/build/resources/images/field.jpg"));
        fieldCanvas = new CanvasMovie(videoFramePanel, SWT.NO_BACKGROUND | SWT.NO_REDRAW_RESIZE);
        fieldCanvas.setSize(698, 460);

        fieldCanvas.addPaintListener(new FieldPaintListner());

        ImageData playerNormal = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/player.png"));
        playerImage = new Image(parent.getDisplay(), playerNormal);

        ImageData playerNormal2 = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/player2.png"));
        playerImage2 = new Image(parent.getDisplay(), playerNormal2);

        ImageData ball = new ImageData(this.getClass().getResourceAsStream("/com/fa13/build/resources/images/ball.png"));
        ballImage = new Image(parent.getDisplay(), ball);

        fieldCanvas.setData("canvas");

        mainComposite.setRedraw(true);

        timeline = new SWTRepaintTimeline(fieldCanvas);
        timeline.setDuration(1000);
        timeline.addPropertyToInterpolate("value", 0.0f, 1.0f);

        teamsInfoPanel = new Composite(mainComposite, SWT.BORDER);
        data = new FormData();
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(0, 0);
        data.bottom = new FormAttachment(100, 0);
        teamsInfoPanel.setLayoutData(data);
        teamsInfoPanel.setLayout(new FormLayout());

        teamsInfoText = new Text(teamsInfoPanel, SWT.MULTI);
        data = new FormData();
        data.left = new FormAttachment(0, 0);
        data.right = new FormAttachment(100, 0);
        data.top = new FormAttachment(0, 0);
        data.bottom = new FormAttachment(100, 0);
        data.width = 500;
        data.top = new FormAttachment(fieldScrolledPanel, 0);
        teamsInfoText.setLayoutData(data);
        teamsInfoText.setEditable(false);
        teamsInfoText.setFont(new Font(parent.getDisplay(), "Monospaced", 10,
                SWT.NONE));
    }

    public class FieldPaintListner implements PaintListener {
        public void paintControl(PaintEvent e) {
            GC gc = e.gc;
            Rectangle rect = fieldImage.getBounds();

            gc.drawImage(fieldImage, (rect.width - 698) / 2,
                    (rect.height - 460) / 2, 698, 460, 0, 0, (int) (698),
                    (int) (460));
            int offsetX = playerImage.getBounds().width / 2;
            int offsetY = playerImage.getBounds().height / 2;
            if (tempVideoFrame != null) {
                for (int i = 0; i < tempVideoFrame.getHomeTeam().size(); i++) {
                    String numberStr = ""
                            + (tempVideoFrame.getFrameTimeInSec() / 60) + ":"
                            + (tempVideoFrame.getFrameTimeInSec() % 60);
                    gc.setFont(timeFont);
                    Color oldColor = gc.getForeground();
                    gc.setForeground(timeColor);
                    Point extent = gc.stringExtent(numberStr);
                    gc.drawString(numberStr, 349 - extent.x / 2, 0, true);
                    gc.setForeground(oldColor);

                    int x = mapVirtToRealX(tempVideoFrame.getHomeTeam().get(i).getX());
                    int y = mapVirtToRealY(tempVideoFrame.getHomeTeam().get(i).getY());
                    if (videoFormInstance.getCurrentFrameIndex() > fieldChange) {
                        x = 698 - x;
                        y = 460 - y;
                    }

                    gc.drawImage(playerImage, -offsetX + x, -offsetY + y);

                    numberStr = videoFormInstance.getTeamsInfo().homePlayers.get(i).getNumber();
                    drawPlayerNumber(gc, playerNumberColorHomeTeam, numberStr, x, y);
                    gc.setForeground(oldColor);

                    x = mapVirtToRealX(tempVideoFrame.getAwayTeam().get(i).getX());
                    y = mapVirtToRealY(tempVideoFrame.getAwayTeam().get(i).getY());
                    if (videoFormInstance.getCurrentFrameIndex() > fieldChange) {
                        x = 698 - x;
                        y = 460 - y;
                    }

                    gc.drawImage(playerImage2, -offsetX + x, -offsetY + y);

                    numberStr = videoFormInstance.getTeamsInfo().awayPlayers.get(i).getNumber();
                    drawPlayerNumber(gc, playerNumberColorAwayTeam, numberStr, x, y);
                    gc.setForeground(oldColor);
                }

                offsetX = ballImage.getBounds().width / 2;
                offsetY = ballImage.getBounds().height / 2;

                int x = mapVirtToRealX(tempVideoFrame.getBall().getX());
                int y = mapVirtToRealY(tempVideoFrame.getBall().getY());
                if (videoFormInstance.getCurrentFrameIndex() > fieldChange) {
                    x = 698 - x;
                    y = 460 - y;
                }

                gc.drawImage(ballImage, -offsetX + x, -offsetY + y);
            }

            rect = new Rectangle(0, 0, 698, 460);
            Rectangle client = fieldCanvas.getClientArea();
            int marginWidth = client.width - rect.width;
            if (marginWidth > 0) {
                gc.fillRectangle(rect.width, 0, marginWidth, client.height);
            }
            int marginHeight = client.height - rect.height;
            if (marginHeight > 0) {
                gc.fillRectangle(0, rect.height, client.width, marginHeight);
            }

            videoFramePanel.redraw();
        }

        private void drawPlayerNumber(GC gc, Color color, String numberStr, int x, int y) {
            Point extent;
            gc.setFont(playerNumberFont);
            gc.setForeground(color);
            extent = gc.stringExtent(numberStr);
            gc.drawString(numberStr, x - extent.x / 2 - 1, y - 14, true);
        }
    }

    public class OpenButtonListner implements SelectionListener {

        public void widgetDefaultSelected(SelectionEvent arg0) {
            openVideoFormDlg();
        }

        public void widgetSelected(SelectionEvent arg0) {
            openVideoFormDlg();
        }
    }

    public class PlayButtonListner implements SelectionListener {

        public void widgetDefaultSelected(SelectionEvent arg0) {
            playOrPauseVideo();
        }

        public void widgetSelected(SelectionEvent arg0) {
            playOrPauseVideo();
        }
    }

    public void openVideoFormDlg() {
        if (play) {
            playOrPauseVideo();
        }

        FileDialog dlg = new FileDialog(parentInstance.getShell(), SWT.OPEN);

        String ext[] = new String[1];
        ext[0] = MainWindow.getMessage("FilterExtensionVideo");
        dlg.setFilterExtensions(ext);
        String extNames[] = new String[1];
        extNames[0] = MainWindow.getMessage("FilterExtensionVideoName");
        dlg.setFilterNames(extNames);
        String result = dlg.open();
        if (result != null) {
            openVideoForm(result);
            updateVideoForm(videoFormInstance);
        }
    }

    public class CanvasMovie extends Canvas {
        CanvasMovie(Composite parent, int mode) {
            super(parent, mode);
        }

        float lastValue = (float) 0.0;

        public void setValue(float newValue) {
            if (lastValue != newValue) {
                if (play && newValue == 0.0 && lastValue != 0.0) {
                    if (videoFormInstance.hasNextFrame()) {
                        previousVideoFrame = currentVideoFrame;
                        currentVideoFrame = videoFormInstance.getNextFrame();
                        for (String addInfo : currentVideoFrame
                                .getAdditionalInfo()) {
                            if (!addInfo.contains("turn")) {
                                matchCommentsText.append(""
                                        + (currentVideoFrame
                                                .getFrameTimeInSec() / 60)
                                        + ":"
                                        + (currentVideoFrame
                                                .getFrameTimeInSec() % 60)
                                        + " " + addInfo);
                            }
                        }
                        videoSlider.setSelection(videoFormInstance
                                .getCurrentFrameIndex());
                    } else {
                        playOrPauseVideo();
                        return;
                    }
                }

                lastValue = newValue;

                tempVideoFrame = new VideoFrame(
                        previousVideoFrame.getFrameTimeInSec()
                                + (int) (newValue * 6));

                Iterator<VideoPlayer> currentIterator = currentVideoFrame
                        .getHomeTeam().iterator();
                Iterator<VideoPlayer> previousIterator = previousVideoFrame
                        .getHomeTeam().iterator();
                while (currentIterator.hasNext() && previousIterator.hasNext()) {
                    VideoPlayer currentVideoPlayer = currentIterator.next();
                    VideoPlayer previousVideoPlayer = previousIterator.next();

                    tempVideoFrame
                            .addHomePlayer(new VideoPlayer(
                                    (int) (previousVideoPlayer.getX() + (currentVideoPlayer
                                            .getX() - previousVideoPlayer
                                            .getX())
                                            * newValue),
                                    (int) (previousVideoPlayer.getY() + (currentVideoPlayer
                                            .getY() - previousVideoPlayer
                                            .getY())
                                            * newValue)));
                }

                currentIterator = currentVideoFrame.getAwayTeam().iterator();
                previousIterator = previousVideoFrame.getAwayTeam().iterator();
                while (currentIterator.hasNext() && previousIterator.hasNext()) {
                    VideoPlayer currentVideoPlayer = currentIterator.next();
                    VideoPlayer previousVideoPlayer = previousIterator.next();

                    tempVideoFrame
                            .addAwayPlayer(new VideoPlayer(
                                    (int) (previousVideoPlayer.getX() + (currentVideoPlayer
                                            .getX() - previousVideoPlayer
                                            .getX())
                                            * newValue),
                                    (int) (previousVideoPlayer.getY() + (currentVideoPlayer
                                            .getY() - previousVideoPlayer
                                            .getY())
                                            * newValue)));
                }

                tempVideoFrame
                        .setBall(new VideoBall((int) (previousVideoFrame
                                .getBall().getX() + (currentVideoFrame
                                .getBall().getX() - previousVideoFrame
                                .getBall().getX())
                                * newValue), (int) (previousVideoFrame
                                .getBall().getY() + (currentVideoFrame
                                .getBall().getY() - previousVideoFrame
                                .getBall().getY())
                                * newValue), (int) (previousVideoFrame
                                .getBall().getY() + (currentVideoFrame
                                .getBall().getY() - previousVideoFrame
                                .getBall().getY())
                                * newValue)));
            }
        }
    }

    public void playOrPauseVideo() {
        if (currentVideoFrame != null) {
            play = !play;

            if (play) {
                if (videoFormInstance.hasNextFrame()) {
                    previousVideoFrame = currentVideoFrame;
                    currentVideoFrame = videoFormInstance.getNextFrame();
                    for (String addInfo : currentVideoFrame.getAdditionalInfo()) {
                        if (!addInfo.contains("turn")) {
                            matchCommentsText
                                    .append(""
                                            + (currentVideoFrame
                                                    .getFrameTimeInSec() / 60)
                                            + ":"
                                            + (currentVideoFrame
                                                    .getFrameTimeInSec() % 60)
                                            + " " + addInfo);
                        }
                    }
                    videoSlider.setSelection(videoFormInstance
                            .getCurrentFrameIndex());

                    timeline.playLoop(RepeatBehavior.LOOP);
                }
            } else {
                timeline.cancel();
            }
        }
    }

    public void openVideoForm(String fname) {
        if (fname == null) {
            return;
        }
        try {
            videoFormInstance = VideoReader.readVideo(fname);
            videoSlider.setMinimum(0);
            videoSlider.setMaximum(videoFormInstance.getVideoFrameNumber() - 1);
            videoSlider.setCursor(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateVideoForm(Video videoFormInstance) {
        StringBuilder teamBlock = new StringBuilder();
        for (VideoPlayerInfo vpi : videoFormInstance.getTeamsInfo().homePlayers) {
            if (!vpi.getInfo().startsWith("НН")) {
                teamBlock.append(vpi.getNumber()).append(" ")
                        .append(vpi.getInfo()).append("\n");
            }
        }
        teamBlock.append("\n");
        for (VideoPlayerInfo vpi : videoFormInstance.getTeamsInfo().awayPlayers) {
            if (!vpi.getInfo().startsWith("НН")) {
                teamBlock.append(vpi.getNumber()).append(" ")
                        .append(vpi.getInfo()).append("\n");
            }
        }

        currentVideoFrame = videoFormInstance.getFirstFrame();
        videoFrameСycle: while (videoFormInstance.hasNextFrame()) {
            currentVideoFrame = videoFormInstance.getNextFrame();
            for (String info : currentVideoFrame.getAdditionalInfo()) {
                if (info.contains("half end")) {
                    fieldChange = videoFormInstance.getCurrentFrameIndex();
                    break videoFrameСycle;
                }
            }
        }
        teamsInfoText.setText(teamBlock.toString());

        currentVideoFrame = videoFormInstance.getFirstFrame();
        tempVideoFrame = currentVideoFrame.clone();
        fieldCanvas.redraw();
    }

    @Override
    public void updateAll() {

    }

    @Override
    public void updatePassword(String password) {

    }

    @Override
    public void updateMessages() {
        mainItem.setText(MainWindow.getMessage("videoTabName"));

        openButton.setText(MainWindow.getMessage("global.open"));

        videoSpeedSlider.setToolTipText(MainWindow.getMessage("video.speed"));

        ImageData playPause = new ImageData(this.getClass()
                .getResourceAsStream(
                        "/com/fa13/build/resources/images/playPause.png"));
        playButton.setImage(new Image(display, playPause));

        bottomPanel.layout();
    }

    @Override
    public void redraw() {

    }

    public int mapVirtToRealX(int x) {
        return (int) (((double) 678.0 * (x + 60) / 120.0) + 10);
    }

    public int mapVirtToRealY(int y) {
        return (int) (((double) 440.0 * (y + 45) / 90.0) + 10);
    }

    private boolean needUpdate = true;
    
    @Override
    public void updateAll(boolean lazyUpdate) {
        // TODO Auto-generated method stub
        needUpdate = lazyUpdate;
    }

    @Override
    public TabItem asTabItem() {
        // TODO Auto-generated method stub
        return mainItem;
    }
    
    private class VideoSliderListener implements SelectionListener {

        public void widgetSelected(SelectionEvent e) {
            if (videoFormInstance != null) {
                matchCommentsText.setText("");
                if (play) {
                    playOrPauseVideo();
                }
                currentVideoFrame = videoFormInstance.getFrame(videoSlider
                        .getSelection());
                tempVideoFrame = currentVideoFrame.clone();
                fieldCanvas.redraw();
            }
        }

        public void widgetDefaultSelected(SelectionEvent e) {
        }
    }

    private class VideoSpeedSliderListener implements SelectionListener {

        public void widgetSelected(SelectionEvent e) {
            if (play) {
                playOrPauseVideo();
            }
            timeline.setDuration(10000 / (videoSpeedSlider.getSelection()));
        }

        public void widgetDefaultSelected(SelectionEvent e) {
        }
    }

    private Font createFont(TabFolder parent, int height, int style) {
        FontData fontData = parent.getFont().getFontData()[0];
        fontData.setHeight(height);
        fontData.setStyle(style);
        return new Font(parent.getDisplay(), fontData);
    }

    @Override
    public void store(Properties props) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub
        
    }
}