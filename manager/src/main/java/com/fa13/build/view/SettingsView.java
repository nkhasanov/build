package com.fa13.build.view;

import java.util.Properties;
import java.util.ResourceBundle;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

import com.fa13.build.utils.GuiUtils;

public class SettingsView extends Dialog {

    Properties settings;
    Properties settingsDef;

    public Properties getSettings() {
        return settings;
    }

    public void setSettings(Properties settings) {
        this.settings = settings;
    }

    public SettingsView(Shell parent) {
        super(parent, SWT.DIALOG_TRIM | SWT.APPLICATION_MODAL);
    }

    Combo langCombo;
    Label langLabel;
    Button okButton;
    Button cancelButton;
    private Label playerSizeLabel;
    private Combo playerSizeCombo;

    public Properties open(ResourceBundle messages, Properties settingsDefault, int x, int y) {
        final Shell shell = new Shell(getParent(), getStyle() | SWT.APPLICATION_MODAL);
        shell.setText(getText());
        shell.setLocation(x, y);
        settingsDef = settingsDefault;
        settings = settingsDefault;

        shell.setLayout(new GridLayout(2, false));
        GridData data = new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false);
        langLabel = new Label(shell, SWT.NONE);
        langLabel.setLayoutData(data);
        data = new GridData(SWT.END, SWT.BEGINNING, true, false);
        data.horizontalIndent=5;
        langCombo = new Combo(shell, SWT.DROP_DOWN | SWT.READ_ONLY);
        langCombo.setLayoutData(data);
        langLabel.setText(messages.getString("settings.language"));

        final Button checkUpdatesOnStartupButton = new Button(shell, SWT.CHECK);
        data = new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false, 2, 1);
        data.horizontalIndent=5;
        checkUpdatesOnStartupButton.setLayoutData(data);
        checkUpdatesOnStartupButton.setText(MainWindow.getMessage("checkUpdatesOnStartup"));

        Object key = settings.get(MainWindow.PROP_CHECK_UPDATES_STARTUP);
        boolean flag = (key != null && key.toString().equals("1"));
        checkUpdatesOnStartupButton.setSelection(flag);

        checkUpdatesOnStartupButton.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                widgetDefaultSelected(e);
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                int flag = checkUpdatesOnStartupButton.getSelection() ? 1 : 0;
                settings.put(MainWindow.PROP_CHECK_UPDATES_STARTUP, String.valueOf(flag));
            }
        });

        final Button checkAllOnStartupButton = new Button(shell, SWT.CHECK);
        data = new GridData(SWT.CENTER, SWT.BEGINNING, true, false, 2, 1);
        data.horizontalIndent=5;
        checkAllOnStartupButton.setLayoutData(data);
        checkAllOnStartupButton.setText(MainWindow.getMessage("checkCurrentAllOnStartup"));

        key = settings.get(MainWindow.PROP_CHECK_CURRENT_ALL_STARTUP);
        flag = (key != null && key.toString().equals("1"));
        checkAllOnStartupButton.setSelection(flag);

        checkAllOnStartupButton.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                widgetDefaultSelected(e);
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                int flag = checkAllOnStartupButton.getSelection() ? 1 : 0;
                settings.put(MainWindow.PROP_CHECK_CURRENT_ALL_STARTUP, String.valueOf(flag));
            }
        });

        data = new GridData(SWT.BEGINNING, SWT.BEGINNING, false, false);
        data.horizontalIndent=5;
        playerSizeLabel = new Label(shell, SWT.NONE);
        playerSizeLabel.setLayoutData(data);
        playerSizeLabel.setText(messages.getString("PlayerImageSize"));
        data = new GridData(SWT.BEGINNING, SWT.BEGINNING, true, false);
        data.horizontalIndent=5;
        playerSizeCombo = new Combo(shell, SWT.DROP_DOWN | SWT.READ_ONLY);
        playerSizeCombo.setLayoutData(data);
        String value = MainWindow.getMessage("Standard");
        playerSizeCombo.add(value);
        playerSizeCombo.setData(value, "Standard");
        value = MainWindow.getMessage("Small");
        playerSizeCombo.add(value);
        playerSizeCombo.setData(value, "Small");

        key = settings.get(MainWindow.PROP_PLAYER_IMAGE_SIZE);
        if (key == null || key.toString().isEmpty() || key.toString().equalsIgnoreCase("Standard")) {
            playerSizeCombo.select(0);
        } else {
            playerSizeCombo.select(1);
        }
        playerSizeCombo.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {

                int selectionIndex = playerSizeCombo.getSelectionIndex();
                String keyToSave = "";
                if (selectionIndex != -1) {
                    keyToSave = (String) playerSizeCombo.getData(playerSizeCombo.getItem(selectionIndex));
                }
                settings.put(MainWindow.PROP_PLAYER_IMAGE_SIZE, keyToSave);
                GuiUtils.showSuccessMessage(shell, MainWindow.getMessage("Warning"), MainWindow.getMessage("PlayerImageSizeChangeWarning"));
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                widgetSelected(e);
            }
        });

        Composite buttonsPanel = new Composite(shell, SWT.NONE);
        buttonsPanel.setLayout(new FillLayout());
        data = new GridData(SWT.CENTER, SWT.BEGINNING, true, false, 2, 1);
        buttonsPanel.setLayoutData(data);

        okButton = new Button(buttonsPanel, SWT.PUSH);
        okButton.setText(messages.getString("global.ok"));
        okButton.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                widgetDefaultSelected(e);
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                shell.close();
            }
        });
        /*        
                cancelButton = new Button(buttonsPanel, SWT.PUSH);
                cancelButton.setText(messages.getString("global.cancel"));
                cancelButton.addSelectionListener(new SelectionListener() {

                    @Override
                    public void widgetSelected(SelectionEvent e) {
                        widgetDefaultSelected(e);
                    }

                    @Override
                    public void widgetDefaultSelected(SelectionEvent e) {
                        settings = settingsDef;
                        shell.close();
                    }
                });
        */
        langCombo.add("Русский");
        langCombo.add("English");
        langCombo.setData("Русский", "/com/fa13/build/resources/properties/MessagesBundle_ru_RU.properties");
        langCombo.setData("English", "/com/fa13/build/resources/properties/MessagesBundle_en_US.properties");

        langCombo.addSelectionListener(new SelectionListener() {

            @Override
            public void widgetSelected(SelectionEvent e) {
                widgetDefaultSelected(e);
            }

            @Override
            public void widgetDefaultSelected(SelectionEvent e) {
                int index = langCombo.getSelectionIndex();
                if (index >= 0) {
                    settings.setProperty("langIndex", String.valueOf(index));
                    String lang = langCombo.getItem(index);
                    String data = (String) langCombo.getData(lang);
                    settings.setProperty("lang", data);
                }
            }
        });
        int defIndex = Integer.valueOf(settingsDefault.getProperty("langIndex", "0"));
        langCombo.select(defIndex);
        shell.setDefaultButton(okButton);
        shell.pack();
        shell.open();
        Display display = getParent().getDisplay();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }
        return settings;
    }
}
