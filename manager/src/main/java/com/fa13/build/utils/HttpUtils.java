package com.fa13.build.utils;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;

import java.io.IOException;

public abstract class HttpUtils {
    private static final HttpConnectionManager CONNECTION_MANAGER = new MultiThreadedHttpConnectionManager();
    private static final HttpClient HTTP_CLIENT = new HttpClient(CONNECTION_MANAGER);

    public static String getRemoteContent(String uri) throws IOException {
        GetMethod method = new GetMethod(uri);
        int statusCode = HTTP_CLIENT.executeMethod(method);
        if (statusCode >= 300 || statusCode < 200) {
            throw new IOException("Could not get content from " + uri + ". Remote host answered HTTP " + statusCode);
        }
        return method.getResponseBodyAsString();
    }

}
