package com.fa13.build.utils;

import com.fa13.build.model.Player;
import com.fa13.build.view.MainWindow;

public class GameUtils {

    public static String getPlayerAbilitiesAsString(Player player) {
        StringBuilder result = new StringBuilder();
        if (player.getShooting() > 20) {
            result.append(MainWindow.getMessage("PTshooting").toLowerCase()).append(player.getShooting());
        }
        if (player.getPassing() > 20) {
            result.append(MainWindow.getMessage("PTpassing").toLowerCase()).append(player.getPassing());
        }
        if (player.getCross() > 20) {
            result.append(MainWindow.getMessage("PTcross").toLowerCase()).append(player.getCross());
        }
        if (player.getDribbling() > 20) {
            result.append(MainWindow.getMessage("PTdribbling").toLowerCase()).append(player.getDribbling());
        }
        if (player.getTackling() > 20) {
            result.append(MainWindow.getMessage("PTtackling").toLowerCase()).append(player.getTackling());
        }
        if (player.getSpeed() > 20) {
            result.append(MainWindow.getMessage("PTspeed").toLowerCase()).append(player.getSpeed());
        }
        if (player.getStamina() > 20) {
            result.append(MainWindow.getMessage("PTstamina").toLowerCase()).append(player.getStamina());
        }
        if (player.getHeading() > 20) {
            result.append(MainWindow.getMessage("PTheading").toLowerCase()).append(player.getHeading());
        }
        if (player.getReflexes() > 20) {
            result.append(MainWindow.getMessage("PTreflexes").toLowerCase()).append(player.getReflexes());
        }
        if (player.getHandling() > 20) {
            result.append(MainWindow.getMessage("PThandling").toLowerCase()).append(player.getHandling());
        }

        return result.toString();
    }

}