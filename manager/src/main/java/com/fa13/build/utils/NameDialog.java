package com.fa13.build.utils;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Dialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.fa13.build.view.MainWindow;

public class NameDialog extends Dialog {

    private int result = SWT.OK;
    private Text nameEditor;

    public NameDialog(Shell parent) {
        this(parent, SWT.BORDER | SWT.APPLICATION_MODAL | SWT.TITLE);
    }

    public NameDialog(Shell parent, int style) {
        super(parent, style);
    }

    public int open() {
        Shell shell = new Shell(getParent(), getStyle());
        shell.setText(getText());
        createContents(shell);

        Rectangle ca = getParent().getShell().getClientArea();
        int dlgW = shell.getBounds().width;
        int dlgH = shell.getBounds().height;
        int x = (ca.width - dlgW) / 2;
        int y = (ca.height - dlgH) / 2;
        shell.setBounds(x, y, dlgW, dlgH);

        shell.pack();
        shell.open();
        Display display = getParent().getDisplay();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch()) {
                display.sleep();
            }
        }
        return result;
    }

    private void createContents(final Shell shell) {
        shell.setLayout(new GridLayout(2, false));
        GridData gd = new GridData(GridData.FILL_HORIZONTAL);

        nameEditor = new Text(shell, SWT.BORDER);
        gd.horizontalSpan = 2;
        //gd.grabExcessHorizontalSpace = true;
        nameEditor.setLayoutData(gd);
        
        final Button bOk = new Button(shell, SWT.PUSH);
        bOk.setText(MainWindow.getMessage("global.continue"));
        gd = new GridData(GridData.FILL_HORIZONTAL);
        bOk.setLayoutData(gd);
        bOk.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent event) {
                result = SWT.OK;
                shell.close();
            }
        });
        bOk.setEnabled(isAllowEmpty());
        Button bCancel = new Button(shell, SWT.PUSH);
        bCancel.setText(MainWindow.getMessage("global.cancel"));
        gd = new GridData(GridData.FILL_HORIZONTAL);
        bCancel.setLayoutData(gd);
        bCancel.addSelectionListener(new SelectionAdapter() {
            public void widgetSelected(SelectionEvent event) {
                result = SWT.CANCEL;
                shell.close();
            }
        });

        nameEditor.addModifyListener(new ModifyListener() {
            
            @Override
            public void modifyText(ModifyEvent e) {
                name = nameEditor.getText();
                bOk.setEnabled(isAllowEmpty() || !name.isEmpty());
            }
        });        
        
        shell.setDefaultButton(bOk);

        FontData fontData[] = bCancel.getFont().getFontData();
        fontData[0].setHeight(12);
        fontData[0].setStyle(SWT.BOLD + SWT.ITALIC);

        shell.pack();
    }
    
    private String name="";
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        nameEditor.setText(name);
    }
    
    public boolean isAllowEmpty() {
        return allowEmpty;
    }

    public void setAllowEmpty(boolean allowEmpty) {
        this.allowEmpty = allowEmpty;
    }

    private boolean allowEmpty = true;

}