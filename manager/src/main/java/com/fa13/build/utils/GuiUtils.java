package com.fa13.build.utils;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Map;
import java.util.Map.Entry;

import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.TabItem;

import com.fa13.build.view.MainWindow;

public class GuiUtils {

    public static final String LINE_BREAK = "\r\n";
    public static final Charset WIN_CHARSET = Charset.forName("Cp1251");
    public static final Charset UTF8_CHARSET = Charset.forName("UTF-8");

    public static void showModalProgressWindow(final Shell parentShell, final GuiLongTask task, String text, boolean useProgressBar) {
        final Shell dialog = new Shell(parentShell, SWT.BORDER | SWT.APPLICATION_MODAL);
        Rectangle ca = parentShell.getClientArea();
        int dlgW = 400;
        int dlgH = 80;
        int x = (ca.width - dlgW) / 2;
        int y = (ca.height - dlgH) / 2;
        dialog.setBounds(x, y, dlgW, dlgH);

        dialog.setLayout(new GridLayout(1, true));
        final Composite contents = new Composite(dialog, SWT.FILL | SWT.BORDER);
        contents.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        GridLayout gl = new GridLayout(1, true);
        contents.setLayout(gl);
        final Label l = new Label(contents, SWT.CENTER);
        l.setText(text);
        l.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
        ProgressBar pb = null;
        if (useProgressBar) {
            pb = new ProgressBar(contents, SWT.HORIZONTAL | SWT.FILL / SWT.INDETERMINATE);
            pb.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, true));
            pb.setMinimum(0);
            pb.setMaximum(100);
        }
        contents.pack();
        contents.layout();
        // dialog.pack();
        dialog.open();

        // dialog.layout();
        task.setOnFinishExecute(new GuiLongTask.IOnFinish() {

            @Override
            public void execute() {
                dialog.close();
                dialog.dispose();
            }
        });
        if (useProgressBar) {
            final ProgressBar pb2 = pb;
            task.setOnProgressExecute(new GuiLongTask.IOnProgress() {

                @Override
                public void progress(final int percent, final String message) {

                    if (!pb2.isDisposed()) {
                        pb2.setSelection(percent);
                        pb2.update();
                        pb2.redraw();
                    }
                    if (!l.isDisposed()) {
                        l.setText(message);
                        l.update();
                    }
                    if (!dialog.isDisposed()) {
                        dialog.layout();
                        dialog.update();
                    }

                }
            });
        } else {
            //only show progress text changed
            task.setOnProgressExecute(new GuiLongTask.IOnProgress() {

                @Override
                public void progress(final int percent, final String message) {

                    if (!l.isDisposed()) {
                        l.setText(message);
                        l.update();
                    }
                    if (!dialog.isDisposed()) {
                        dialog.layout();
                        dialog.update();
                    }

                }
            });

        }
        task.start();

    }

    public static void showErrorMessage(Shell shell, String title, String text) {
        MessageBox dlg = new MessageBox(shell, SWT.ICON_ERROR | SWT.OK);
        dlg.setText(title);
        dlg.setMessage(text);
        dlg.open();
    }

    public static void showSuccessMessage(Shell shell, String title, String text) {
        MessageBox dlg = new MessageBox(shell, SWT.ICON_INFORMATION | SWT.OK);
        dlg.setText(title);
        dlg.setMessage(text);
        dlg.open();
    }

    // to open URI in external browser
    public static void openWebpage(URI uri) {
        Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(uri);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // to open URL in external browser
    public static void openWebpage(URL url) {
        try {
            openWebpage(url.toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public static void openWebpage(String urlString) {
        try {
            openWebpage(new URL(urlString));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public static final int DOWNLOAD_CONNECTION_TIMEOUT = 10 * 1000;

    public static void downloadUrlResource(String imageUrl, String destinationFile) throws Exception {

        File fileToSave = new File(destinationFile);
        if (!fileToSave.getParentFile().exists()) {
            fileToSave.getParentFile().mkdirs();
        }
        fileToSave.createNewFile();
        URL url = new URL(imageUrl);
        InputStream is = url.openStream();
        OutputStream os = new FileOutputStream(fileToSave);

        byte[] b = new byte[2048];
        int length;

        while ((length = is.read(b)) != -1) {
            os.write(b, 0, length);
        }

        is.close();
        os.close();
    }

    /**
     * Updates various text controls such as Label, Button etc
     * 
     */
    public static void updateTextControls(Map<Object, String> textControlsToUpdate) {

        if (textControlsToUpdate != null && textControlsToUpdate.entrySet() != null)
            for (Map.Entry<Object, String> entry : textControlsToUpdate.entrySet()) {
                String text = MainWindow.getMessage(entry.getValue());
                if (entry.getKey() instanceof Label) {
                    ((Label) entry.getKey()).setText(text);
                } else if (entry.getKey() instanceof Button) {
                    ((Button) entry.getKey()).setText(text);
                } else if (entry.getKey() instanceof TabItem) {
                    ((TabItem) entry.getKey()).setText(text);
                } else if (entry.getKey() instanceof Group) {
                    ((Group) entry.getKey()).setText(text);
                }

            }

    }

    public static int getComboItemIndex(Combo combo, String itemName) {

        for (int i = 0; i < combo.getItemCount(); i++) {
            if (combo.getItem(i).equals(itemName)) {
                return i;
            }
        }
        return -1;
    }

    public static <T, E> T getKeyByValue(Map<T, E> map, E value) {
        for (Entry<T, E> entry : map.entrySet()) {
            if (value.equals(entry.getValue())) {
                return entry.getKey();
            }
        }
        return null;
    }

    public static Font createFont(Font parentFont, int height, int style) {
        FontData fontData = parentFont.getFontData()[0];
        fontData.setHeight(height);
        fontData.setStyle(style);
        return new Font(parentFont.getDevice(), fontData);
    }

}