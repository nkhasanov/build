package com.fa13.build.utils;


import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import org.apache.commons.io.FileUtils;

public abstract class ResourceUtils {

    public static BufferedImage getClasspathResourceAsImage(String name) throws IOException {
        return ImageIO.read(ResourceUtils.class.getResource(name));
    }

    public static String getClasspathResourceAsString(String name) throws IOException {
        File file = FileUtils.toFile(ResourceUtils.class.getResource(name));
        return FileUtils.readFileToString(file, "utf8");
    }

    public static String getClasspathResourceAbsolutePath(String name) throws IOException {
        File file = FileUtils.toFile(ResourceUtils.class.getResource(name));
        return file.getAbsolutePath();
    }

    public static ImageIcon getClasspathResourceAsImageIcon(String path) throws IOException {
        BufferedImage image = getClasspathResourceAsImage(path);
        if (image != null) {
            return new ImageIcon(image);
        } else {
            throw new IOException("Cannot find image: " + path);
        }
    }
}