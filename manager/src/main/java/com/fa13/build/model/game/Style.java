package com.fa13.build.model.game;


import com.fa13.build.view.MainWindow;

public enum Style {
    WING_PLAY  ("фланг",   "Style.wingPlay"),
    COMBINATION("комбина", "Style.combination"),
    CENTER     ("центр",   "Style.center"),
    ENGLAND    ("англия",  "Style.england"),
    LONG_BALL  ("длина",   "Style.longBall"),
    UNIVERSAL  ("универ",  "Style.universal");

    private final String formValue;
    private final String localizationKey;

    private Style(String s, String localizationKey) {
        formValue = s;
        this.localizationKey = localizationKey;
    }

    public static Style resolveByFormValue(String formValue) {
        for (Style s : values()) {
            if (s.formValue.equals(formValue)) {
                return s;
            }
        }
        return null;
    }

    public String getFormValue() {
        return formValue;
    }

    public String getLocalizedString() {
        return MainWindow.getMessage(localizationKey);
    }

    public String toString() {
        return formValue;
    }
}