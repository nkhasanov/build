package com.fa13.build.model;

import java.util.Map;

import com.fa13.build.controller.io.ReaderUtils;

public class Teams {

    public static final String DEFAULT_TEAMS_RESOURCE = "/com/fa13/build/resources/game/DefaultTeams.txt";
    private static Map<String, String> defaultTeams;

    public static final String DEFAULT_TEAMS_WORLD_RESOURCE = "/com/fa13/build/resources/game/DefaultTeams_WORLD.txt";
    private static Map<String, String> defaultWorldTeams;

    public static final String DEFAULT_TEAMS_WORLD_U21_RESOURCE = "/com/fa13/build/resources/game/DefaultTeams_WORLD_U21.txt";
    private static Map<String, String> defaultWorldU21Teams;

    private static Map<String, String> defaultAllTeams;
    
    private static String PROPERTY_DELIMITER = "=";

    public static Map<String, String> getDefaultTeams() {
        if (defaultTeams == null) {
            defaultTeams = ReaderUtils.readProperties(DEFAULT_TEAMS_RESOURCE, PROPERTY_DELIMITER);
        }
        return defaultTeams;
    }
    
    public static Map<String, String> getDefaultWorldTeams() {
        if (defaultWorldTeams == null) {
            defaultWorldTeams = ReaderUtils.readProperties(DEFAULT_TEAMS_WORLD_RESOURCE, PROPERTY_DELIMITER);
        }
        return defaultWorldTeams;
    }

    public static Map<String, String> getDefaultWorldU21Teams() {
        if (defaultWorldU21Teams == null) {
            defaultWorldU21Teams = ReaderUtils.readProperties(DEFAULT_TEAMS_WORLD_U21_RESOURCE, PROPERTY_DELIMITER);
        }
        return defaultWorldU21Teams;
    }

    public static Map<String, String> getDefaultAllTeams() {
        if (defaultAllTeams == null) {
            defaultAllTeams = getDefaultTeams();
            defaultAllTeams.putAll(getDefaultWorldTeams());
            defaultAllTeams.putAll(getDefaultWorldU21Teams());
        }
        return defaultAllTeams;

    }
    
}