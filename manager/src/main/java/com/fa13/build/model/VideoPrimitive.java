package com.fa13.build.model;

public interface VideoPrimitive {
    public int getX();
    public int getY();
}
