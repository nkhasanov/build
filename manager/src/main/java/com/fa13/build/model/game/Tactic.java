package com.fa13.build.model.game;


import com.fa13.build.view.MainWindow;

public enum Tactic {
    DEFENCE("защита", "Tactic.defence"),
    BALANCE("баланс", "Tactic.balance"),
    ATTACK("атака", "Tactic.attack");

    private final String formValue;
    private final String localizationKey;

    private Tactic(String formValue, String localizationKey) {
        this.formValue = formValue;
        this.localizationKey = localizationKey;
    }

    public static Tactic resolveByFormValue(String formValue) {
        for (Tactic t : values()) {
            if (t.formValue.equals(formValue)) {
                return t;
            }
        }
        return null;
    }

    public String getFormValue() {
        return formValue;
    }

    public String getLocalizedString() {
        return MainWindow.getMessage(localizationKey);
    }

    public String toString() {
        return formValue;
    }
}