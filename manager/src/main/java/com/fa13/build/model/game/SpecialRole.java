package com.fa13.build.model.game;


public enum SpecialRole {
    CAPTAIN          (0b100000, "к"),
    PENALTY          (0b010000, "п"),
    FREEKICK         (0b001000, "ш"),
    INDIRECT_FREEKICK(0b000100, "с"),
    LEFT_CORNER      (0b000010, "ул"),
    RIGHT_CORNER     (0b000001, "уп");

    private final int code;
    private final String formValue;

    private SpecialRole(int code, String formValue) {
        this.code = code;
        this.formValue = formValue;
    }

    public static SpecialRole resolveByFormValue(String value) {
        for (SpecialRole role : values()) {
            if (role.formValue.equals(value)) {
                return role;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public int getAssistantCode() {
        return code << 6;
    }

    public String getFormValue() {
        return formValue;
    }

    @Override
    public String toString() {
        return formValue;
    }
}
