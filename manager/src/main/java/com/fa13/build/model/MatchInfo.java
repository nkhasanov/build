package com.fa13.build.model;

import org.apache.commons.lang.StringUtils;

public class MatchInfo {

    private String competitionCode;
    private String competitionRound;

    private String competitionCodeRound;

    private String videoUrl;
    private String reportUrl;
    private String homeTeamCode;
    private String awayTeamCode;

    private String homeTeamName;
    private String awayTeamName;

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
        setHomeTeamCode(extractHomeTeamCode(videoUrl));
        setAwayTeamCode(extractAwayTeamCode(videoUrl));
        setCompetitionCodeRound(extractCompetitionCode(videoUrl));
    }

    public String getReportUrl() {
        return reportUrl;
    }

    public void setReportUrl(String reportUrl) {
        this.reportUrl = reportUrl;
        setHomeTeamCode(extractHomeTeamCode(reportUrl));
        setAwayTeamCode(extractAwayTeamCode(reportUrl));
        setCompetitionCodeRound(extractCompetitionCode(reportUrl));
    }

    public String getHomeTeamCode() {
        return homeTeamCode;
    }

    public void setHomeTeamCode(String homeTeamCode) {
        this.homeTeamCode = homeTeamCode;
    }

    public String getAwayTeamCode() {
        return awayTeamCode;
    }

    public void setAwayTeamCode(String awayTeamCode) {
        this.awayTeamCode = awayTeamCode;
    }

    public String getCompetitionCode() {
        return competitionCode;
    }

    public void setCompetitionCode(String competitionCode) {
        this.competitionCode = competitionCode;
    }

    @Override
    public boolean equals(Object obj) {

        if (obj == null) {
            return false;
        }

        if (!(obj instanceof MatchInfo)) {
            return false;
        }

        MatchInfo other = (MatchInfo) obj;
        if (!StringUtils.defaultString(competitionCodeRound).equals(StringUtils.defaultString(other.getCompetitionCodeRound()))) {
            return false;
        }

        if (!StringUtils.defaultString(this.homeTeamCode).equals(StringUtils.defaultString(other.getHomeTeamCode()))) {
            return false;
        }

        if (!StringUtils.defaultString(this.awayTeamCode).equals(StringUtils.defaultString(other.getAwayTeamCode()))) {
            return false;
        }

        return true;

    }

    public String getHomeTeamName() {
        return homeTeamName;
    }

    public void setHomeTeamName(String homeTeamName) {
        this.homeTeamName = homeTeamName;
    }

    public String getAwayTeamName() {
        return awayTeamName;
    }

    public void setAwayTeamName(String awayTeamName) {
        this.awayTeamName = awayTeamName;
    }

    public static String extractHomeTeamCode(String videoOrReportUrl) {

        int start = videoOrReportUrl.length() - 11;
        return videoOrReportUrl.substring(start, start + 3);
    }

    public static String extractAwayTeamCode(String videoOrReportUrl) {

        int start = videoOrReportUrl.length() - 7;
        return videoOrReportUrl.substring(start, start + 3);
    }

    public static String extractCompetitionCode(String videoOrReportUrl) {

        int end = videoOrReportUrl.lastIndexOf('/');
        int start;
        for (start = end - 1; start > -1; start--) {
            if (videoOrReportUrl.charAt(start) == '/') {
                break;
            }
        }
        return videoOrReportUrl.substring(start + 1, end);
    }

    public void extractCompetitionAndRound() {

        int i;
        for (i = competitionCodeRound.length() - 1; i > 2; i--) {
            if (!(competitionCodeRound.charAt(i) >= '0' && competitionCodeRound.charAt(i) <= '9')) {
                break;
            }
        }

        competitionRound = competitionCodeRound.substring(i + 1);
        competitionCode = competitionCodeRound.substring(0, i + 1);

    }

    public String getCompetitionCodeRound() {
        return competitionCodeRound;
    }

    public void setCompetitionCodeRound(String competitionCodeRound) {
        this.competitionCodeRound = competitionCodeRound;
        extractCompetitionAndRound();
    }

    public String getCompetitionRound() {
        return competitionRound;
    }

    public void setCompetitionRound(String competitionRound) {
        this.competitionRound = competitionRound;
    }

}