package com.fa13.build.model.game;


import com.fa13.build.view.MainWindow;

public enum PassingStyle {
    PS_BOTH   ("0", "PassingStyle.both"),
    PS_EXACT  ("1", "PassingStyle.exact"),
    PS_FORWARD("2", "PassingStyle.forward");

    private final String formValue;
    private final String localizationKey;

    private PassingStyle(String formValue, String localizationKey) {
        this.formValue = formValue;
        this.localizationKey = localizationKey;
    }

    public static PassingStyle resolveByFormValue(String formValue) {
        for (PassingStyle action : values()) {
            if (action.formValue.equals(formValue)) {
                return action;
            }
        }
        return null;
    }

    public String getFormValue() {
        return formValue;
    }

    public String getLocalizedString() {
        return MainWindow.getMessage(localizationKey);
    }

    @Override
    public String toString() {
        return name();
    }
}