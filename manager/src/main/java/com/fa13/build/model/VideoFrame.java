package com.fa13.build.model;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class VideoFrame {
    private List<VideoPlayer> homeTeam = new ArrayList<VideoPlayer>();
    private List<VideoPlayer> awayTeam = new ArrayList<VideoPlayer>();
    private VideoBall ball;
    private List<String> additionalInfo = new LinkedList<String>();
    private final int frameTimeInSec;

    public int getFrameTimeInSec() {
        return frameTimeInSec;
    }

    public VideoFrame(int timeInSec) {
        frameTimeInSec = timeInSec;
    }

    public List<VideoPlayer> getHomeTeam() {
        return homeTeam;
    }
    public void addHomePlayer(VideoPlayer videoPlayer) {
        homeTeam.add(videoPlayer);
    }

    public List<VideoPlayer> getAwayTeam() {
        return awayTeam;
    }
    public void addAwayPlayer(VideoPlayer videoPlayer) {
        awayTeam.add(videoPlayer);
    }

    public VideoBall getBall() {
        return ball;
    }
    public void setBall(VideoBall ball) {
        this.ball = ball;
    }
    public List<String> getAdditionalInfo() {
        return additionalInfo;
    }
    public void addAdditionalInfo(String additionalInfo) {
        this.additionalInfo.add(additionalInfo);
    }

    public VideoFrame clone() {
        VideoFrame result = new VideoFrame(frameTimeInSec);
        for (VideoPlayer vPlayer : getHomeTeam()) {
            result.addHomePlayer(new VideoPlayer(vPlayer.getX(), vPlayer.getY()));
        }
        for (VideoPlayer vPlayer : getAwayTeam()) {
            result.addAwayPlayer(new VideoPlayer(vPlayer.getX(), vPlayer.getY()));
        }
        result.setBall(new VideoBall(getBall().getX(), getBall().getY(), getBall().getHeight()));
        return result;
    }
}
