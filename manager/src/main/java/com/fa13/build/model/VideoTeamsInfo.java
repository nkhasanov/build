package com.fa13.build.model;

import java.util.ArrayList;
import java.util.List;

public class VideoTeamsInfo {
    public List<VideoPlayerInfo> homePlayers = new ArrayList<VideoPlayerInfo>(18);
    public List<VideoPlayerInfo> awayPlayers = new ArrayList<VideoPlayerInfo>(18);
}
