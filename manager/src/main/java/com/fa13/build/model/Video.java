package com.fa13.build.model;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Video {
    private List<VideoFrame> frames = new LinkedList<VideoFrame>();
    private ListIterator<VideoFrame> iterator;
    private VideoTeamsInfo teamsInfo = null;

    public VideoTeamsInfo getTeamsInfo() {
        return teamsInfo;
    }

    public void setTeamsInfo(VideoTeamsInfo teamInfo) {
        this.teamsInfo = teamInfo;
    }

    public VideoFrame getFirstFrame() {
        iterator = frames.listIterator();
        if (iterator.hasNext()) {
            return iterator.next();
        }
        return null;
    }

    public boolean hasNextFrame() {
        return iterator.hasNext();
    }

    public VideoFrame getNextFrame() {
        return iterator.next();
    }

    public VideoFrame getPreviousFrame() {
        return iterator.previous();
    }

    public VideoFrame getFrame(int index) {
        iterator = frames.listIterator(index);
        return iterator.next();
    }

    public void addFrame(VideoFrame videoFrame) {
        frames.add(videoFrame);
    }

    public int getVideoFrameNumber() {
        return frames.size();
    }

    public int getCurrentFrameIndex() {
        return iterator.previousIndex();
    }
}
