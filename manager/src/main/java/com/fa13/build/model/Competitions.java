package com.fa13.build.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.fa13.build.view.MainWindow;
import com.fa13.build.view.MatchVideoOnlineView;

public class Competitions {

    //warning: don't reorder this items as it affects getBidDefaultFileName()
    public static final String[] ROUND_TYPES = { "тур-1", "тур-2", "тур-3", "тур-4", "тур-5", "тур-6", "тур-7", "тур-8", "тур-9", "тур-10", "тур-11",
            "тур-12", "тур-13", "тур-14", "тур-15", "тур-16", "тур-17", "тур-18", "тур-19", "тур-20", "тур-21", "тур-22", "тур-23", "тур-24",
            "тур-25", "тур-26", "тур-27", "тур-28", "тур-29", "тур-30", "квалиф - 1 игра", "квалиф - 2 игра", "1/64 ф - 1 игра", "1/64 ф - 2 игра",
            "1/32 ф - 1 игра", "1/32 ф - 2 игра", "1/16 ф - 1 игра", "1/16 ф - 2 игра", "1/8 ф - 1 игра", "1/8 ф - 2 игра", "1/4 ф - 1 игра",
            "1/4 ф - 2 игра", "1/2 ф - 1 игра", "1/2 ф - 2 игра", "финал" };
    //used to get roundNum in getBidDefaultFileName()
    public static final Map<String, Integer> ROUND_TYPE_NUMBER = new HashMap<String, Integer>();
    static {
        int fileRound = -1;
        for (int i = 0; i < ROUND_TYPES.length; i++) {
            if (i < 30) {
                fileRound = i + 1;
            } else {
                switch (i) {
                    case 30:
                        fileRound = 14;
                        break;
                    case 31:
                        fileRound = 13;
                        break;
                    case 32:
                        fileRound = 12;
                        break;
                    case 33:
                        fileRound = 11;
                        break;
                    case 34:
                        fileRound = 10;
                        break;
                    case 35:
                        fileRound = 9;
                        break;
                    case 36:
                        fileRound = 8;
                        break;
                    case 37:
                        fileRound = 7;
                        break;
                    case 38:
                        fileRound = 6;
                        break;
                    case 39:
                        fileRound = 5;
                        break;
                    case 40:
                        fileRound = 4;
                        break;
                    case 41:
                        fileRound = 3;
                        break;
                    case 42:
                        fileRound = 2;
                        break;
                    case 43:
                        fileRound = 1;
                        break;
                    case 44:
                        fileRound = 0;
                        break;
                }
            }
            ROUND_TYPE_NUMBER.put(ROUND_TYPES[i], Integer.valueOf(fileRound));
        }
    }

    public static String getMatchRoundName(String competitionCode, int roundNumber) {
        // do big case here for future changes
        String res = "";
        if (competitionCode.equals("cPM")) {
            //13,14
            res = ROUND_TYPES[30 + 14 - roundNumber];

        } else if (competitionCode.equals("rfg")) {

            res = MainWindow.getMessage("dayOfMonth") + " - " + roundNumber;

        } else if (WORLD_COMPETITIONS.indexOf(competitionCode) != -1) {

            if (competitionCode.startsWith("c")) {
                res = ROUND_TYPES[30 + 14 - roundNumber];
            } else {
                res = ROUND_TYPES[roundNumber - 1];
            }

        } else if (WORLD_U21_COMPETITIONS.indexOf(competitionCode) != -1) {

            if (competitionCode.startsWith("c")) {
                res = ROUND_TYPES[30 + 14 - roundNumber];
            } else {
                res = ROUND_TYPES[roundNumber - 1];
            }

        } else if (INTERNATIONAL_COMPETITIONS.indexOf(competitionCode) != -1) {

            if (competitionCode.startsWith("c")) {
                res = ROUND_TYPES[30 + 14 - roundNumber];
            } else {
                res = ROUND_TYPES[roundNumber - 1];
            }

        } else if (competitionCode.matches("r\\d+")) {

            res = ROUND_TYPES[roundNumber - 1];

        } else if (competitionCode.startsWith("c")) {
            //NATIONAL CUP
            res = ROUND_TYPES[30 + 14 - roundNumber];

        } else {
            res = ROUND_TYPES[roundNumber - 1];
        }

        return res;
    }

    public enum CompetitionStage {
        QUALIFICATION, PREPHASE_ROUND, PREPHASE_GROUP, GROUP, FINAL, TRANSITIONAL, LAST_GAMES, ALL_GAMES;

        @Override
        public String toString() {
            String key = null;
            switch (this) {
                case QUALIFICATION:
                    key = "stage.qualification";
                    break;
                case PREPHASE_ROUND:
                    key = "stage.prephaseRound";
                    break;
                case PREPHASE_GROUP:
                    key = "stage.prephaseGroup";
                    break;
                case GROUP:
                    key = "stage.group";
                    break;
                case FINAL:
                    key = "stage.final";
                    break;
                case TRANSITIONAL:
                    key = "stage.transitional";
                    break;
                case LAST_GAMES:
                    key = "stage.lastGames";
                    break;
                case ALL_GAMES:
                    key = "stage.allGames";
                    break;
                default:
                    break;
            }

            return MainWindow.getMessage(key);
        }
    }

    //Чемп
    //http://www.fa13.info/champ.html?champ=Ge&a=r
    //http://www.fa13.info/champ.html?champ=Ge&a=c
    //ЛЧ
    //ЛЧ-квали
    //http://www.fa13.info/ik.html?ik=CL&a=q
    //ЛЧ-игры в группах
    //http://www.fa13.info/ik.html?ik=CL&a=r
    //ЛЧ-финальная часть
    //http://www.fa13.info/ik.html?ik=CL&a=c
    //КА-предварит.раунд
    //http://www.fa13.info/ik.html?ik=UC&a=p
    //КА-квалифик = НЕТ !!!
    //http://www.fa13.info/ik.html?ik=UC&a=q
    //КА-игры в группах
    //http://www.fa13.info/ik.html?ik=UC&a=r
    //КА-финальная
    //http://www.fa13.info/ik.html?ik=UC&a=c
    //КФ - квали, основные, финальная
    //http://www.fa13.info/ik.html?ik=FC&a=q
    //http://www.fa13.info/ik.html?ik=FC&a=r
    //http://www.fa13.info/ik.html?ik=FC&a=c
    //ЧМ отборочные
    //http://www.fa13.info/WC.html?r
    //ЧМ групповой
    //http://www.fa13.info/WC.html?rm
    //ЧМ финальная
    //http://www.fa13.info/WC.html?w
    //ЮЧМ-групповой
    //http://www.fa13.info/WCy.html?r
    //ЮЧМ-финальная
    //http://www.fa13.info/WCy.html?c
    public static final String DEFAULT_COMPETITIONS_RESOURCE = "/com/fa13/build/resources/game/DefaultCompetitions.txt";
    private static Map<String, String> defaultCompetitions;

    public static Map<String, String> getDefaultCompetitions() {
        if (defaultCompetitions == null) {
            initDefaultCompetitions();
        }
        return defaultCompetitions;
    }

    public static final Charset UTF8_CHARSET = Charset.forName("UTF-8");

    private static void initDefaultCompetitions() {
        if (defaultCompetitions != null) {
            return;
        }
        defaultCompetitions = new TreeMap<String, String>();
        BufferedReader br = new BufferedReader(new InputStreamReader(MatchVideoOnlineView.class.getResourceAsStream(DEFAULT_COMPETITIONS_RESOURCE),
                UTF8_CHARSET));

        try {
            String s = br.readLine();
            while (s != null) {

                String ss[] = s.split("=");
                defaultCompetitions.put(ss[0], ss[1]);
                //System.out.println(ss[0] + "=" + ss[1]);
                s = br.readLine();
            }
            //WORLD CUP
            WORLD_COMPETITIONS.add("rWA");
            WORLD_COMPETITIONS.add("rWB");
            WORLD_COMPETITIONS.add("rWC");
            WORLD_COMPETITIONS.add("rWD");
            WORLD_COMPETITIONS.add("rWE");
            WORLD_COMPETITIONS.add("rWF");
            WORLD_COMPETITIONS.add("rWG");
            WORLD_COMPETITIONS.add("rWH");
            WORLD_COMPETITIONS.add("rWI");
            WORLD_COMPETITIONS.add("rWJ");
            WORLD_COMPETITIONS.add("rWK");
            WORLD_COMPETITIONS.add("rWL");

            WORLD_GROUP_COMPETITIONS.add("rWA");
            WORLD_GROUP_COMPETITIONS.add("rWB");
            WORLD_GROUP_COMPETITIONS.add("rWC");
            WORLD_GROUP_COMPETITIONS.add("rWD");
            WORLD_GROUP_COMPETITIONS.add("rWE");
            WORLD_GROUP_COMPETITIONS.add("rWF");
            WORLD_GROUP_COMPETITIONS.add("rWG");
            WORLD_GROUP_COMPETITIONS.add("rWH");
            WORLD_GROUP_COMPETITIONS.add("rWI");
            WORLD_GROUP_COMPETITIONS.add("rWJ");
            WORLD_GROUP_COMPETITIONS.add("rWK");
            WORLD_GROUP_COMPETITIONS.add("rWL");

            WORLD_COMPETITIONS.add("rmA");
            WORLD_COMPETITIONS.add("rmB");
            WORLD_COMPETITIONS.add("rmC");
            WORLD_COMPETITIONS.add("rmD");
            WORLD_COMPETITIONS.add("rmE");
            WORLD_COMPETITIONS.add("rmF");
            WORLD_COMPETITIONS.add("rmG");
            WORLD_COMPETITIONS.add("rmH");

            WORLD_GROUP_COMPETITIONS.add("rmA");
            WORLD_GROUP_COMPETITIONS.add("rmB");
            WORLD_GROUP_COMPETITIONS.add("rmC");
            WORLD_GROUP_COMPETITIONS.add("rmD");
            WORLD_GROUP_COMPETITIONS.add("rmE");
            WORLD_GROUP_COMPETITIONS.add("rmF");
            WORLD_GROUP_COMPETITIONS.add("rmG");
            WORLD_GROUP_COMPETITIONS.add("rmH");

            WORLD_COMPETITIONS.add("cww");
            //WORLD CUP U21
            WORLD_U21_COMPETITIONS.add("ryA");
            WORLD_U21_COMPETITIONS.add("ryB");
            WORLD_U21_COMPETITIONS.add("ryC");
            WORLD_U21_COMPETITIONS.add("ryD");
            WORLD_U21_COMPETITIONS.add("ryE");
            WORLD_U21_COMPETITIONS.add("ryF");
            WORLD_U21_COMPETITIONS.add("ryG");

            WORLD_U21_GROUP_COMPETITIONS.add("ryA");
            WORLD_U21_GROUP_COMPETITIONS.add("ryB");
            WORLD_U21_GROUP_COMPETITIONS.add("ryC");
            WORLD_U21_GROUP_COMPETITIONS.add("ryD");
            WORLD_U21_GROUP_COMPETITIONS.add("ryE");
            WORLD_U21_GROUP_COMPETITIONS.add("ryF");
            WORLD_U21_GROUP_COMPETITIONS.add("ryG");

            WORLD_U21_COMPETITIONS.add("cyy");
            //Champions League
            INTERNATIONAL_COMPETITIONS.add("cCL");
            INTERNATIONAL_COMPETITIONS.add("rLA");
            INTERNATIONAL_COMPETITIONS.add("rLB");
            INTERNATIONAL_COMPETITIONS.add("rLC");
            INTERNATIONAL_COMPETITIONS.add("rLD");
            INTERNATIONAL_COMPETITIONS.add("rLE");
            INTERNATIONAL_COMPETITIONS.add("rLF");
            INTERNATIONAL_COMPETITIONS.add("rLG");
            INTERNATIONAL_COMPETITIONS.add("rLH");

            INTERNATIONAL_GROUP_COMPETITIONS.add("rLA");
            INTERNATIONAL_GROUP_COMPETITIONS.add("rLB");
            INTERNATIONAL_GROUP_COMPETITIONS.add("rLC");
            INTERNATIONAL_GROUP_COMPETITIONS.add("rLD");
            INTERNATIONAL_GROUP_COMPETITIONS.add("rLE");
            INTERNATIONAL_GROUP_COMPETITIONS.add("rLF");
            INTERNATIONAL_GROUP_COMPETITIONS.add("rLG");
            INTERNATIONAL_GROUP_COMPETITIONS.add("rLH");

            //KA
            INTERNATIONAL_COMPETITIONS.add("cUC");
            INTERNATIONAL_COMPETITIONS.add("rUA");
            INTERNATIONAL_COMPETITIONS.add("rUB");
            INTERNATIONAL_COMPETITIONS.add("rUC");
            INTERNATIONAL_COMPETITIONS.add("rUD");
            INTERNATIONAL_COMPETITIONS.add("rUE");
            INTERNATIONAL_COMPETITIONS.add("rUF");
            INTERNATIONAL_COMPETITIONS.add("rUG");
            INTERNATIONAL_COMPETITIONS.add("rUH");

            INTERNATIONAL_GROUP_COMPETITIONS.add("rUA");
            INTERNATIONAL_GROUP_COMPETITIONS.add("rUB");
            INTERNATIONAL_GROUP_COMPETITIONS.add("rUC");
            INTERNATIONAL_GROUP_COMPETITIONS.add("rUD");
            INTERNATIONAL_GROUP_COMPETITIONS.add("rUE");
            INTERNATIONAL_GROUP_COMPETITIONS.add("rUF");
            INTERNATIONAL_GROUP_COMPETITIONS.add("rUG");
            INTERNATIONAL_GROUP_COMPETITIONS.add("rUH");

            //KF
            INTERNATIONAL_COMPETITIONS.add("cFC");

            defaultCompetitions.put("Чемпионат мира", WORLD_CODE);
            defaultCompetitions.put("Мол.Чемпионат мира", WORLD_U21_CODE);

            WORLD_COMPETITIONS.add(WORLD_CODE);
            WORLD_U21_COMPETITIONS.add(WORLD_U21_CODE);

        } catch (Exception ex) {
            //System.out.println(ex.getLocalizedMessage());
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public static final boolean isGroupCompetition(String competitionCode) {
        if (WORLD_GROUP_COMPETITIONS.indexOf(competitionCode) != -1) {
            return true;
        }
        if (WORLD_U21_GROUP_COMPETITIONS.indexOf(competitionCode) != -1) {
            return true;
        }
        if (INTERNATIONAL_GROUP_COMPETITIONS.indexOf(competitionCode) != -1) {
            return true;
        }
        return false;
    }

    public static final String WORLD_CODE = "WORLD";
    public static final String WORLD_U21_CODE = "WORLD_U21";

    public static final String WORLD_FINAL_CODE = "cww";
    public static final String WORLD_U21_FINAL_CODE = "cyy";

    public static final String CHAMPIONS_LEAGUE_CODE = "cCL";
    public static final String ASSOCIATION_CUP_CODE = "cUC";
    public static final String FEDERATION_CUP_CODE = "cFC";

    public static final String URL_COMPETITION_NATIONAL = "http://www.fa13.info/champ.html?champ=<1>&a=<2>";
    //{1}=chamnCode 
    //{2}=(l,r,c) l - last games,  r - all results, c - national cup all results
    public static final String URL_COMPETITION_INTERNATIONAL = "http://www.fa13.info/ik.html?ik=<1>&a=<2>";
    //{1}=internationalCode, (CL,UC,FC)
    //{2}=(l,r,c,q,p) l - last games,  r - all results (main stage), c - final stage all results
    // q-qualification, p - prephase stage

    public static final String URL_COMPETITION_WORLD = "http://www.fa13.info/WC.html?<2>";
    //{1}=(l,r,rm,w)
    //r - prephase group games, rm-group, w - finals
    public static final String URL_COMPETITION_WORLD_U21 = "http://www.fa13.info/WCy.html?<2>";
    //{1}=(l,r,c)

    //l - last games, r - group games, rm -final group games, c - final 

    public static final String URL_COMPETITION_TRANSITIONAL = "http://www.fa13.info/PM.html?<2>";

    //{1}=(l,r)

    public static final String URL_COMPETITION_FRIENDLY = "http://www.fa13.info/fg.html?<2>";

    public static final String URL_COMPETITION_BUSINESS_CUP = "http://www.fa13.info/kt.html?kt=<1>&a=<2>";

    public static List<String> WORLD_COMPETITIONS = new ArrayList<String>();
    public static List<String> WORLD_U21_COMPETITIONS = new ArrayList<String>();

    public static List<String> WORLD_GROUP_COMPETITIONS = new ArrayList<String>();
    public static List<String> WORLD_U21_GROUP_COMPETITIONS = new ArrayList<String>();

    public static List<String> INTERNATIONAL_COMPETITIONS = new ArrayList<String>();
    public static List<String> INTERNATIONAL_GROUP_COMPETITIONS = new ArrayList<String>();

    public static String getCompetitionUrl(String competitionCode, CompetitionStage competitionStage) {
        //System.out.println("getCompetitionUrl(" + competitionCode + "," + competitionStage.name() + ")");
        String urlString = URL_COMPETITION_TRANSITIONAL;
        String p1 = "", p2 = "";
        if (competitionCode == null || competitionCode.trim().isEmpty() || competitionStage == null) {
            return null;
        }
        switch (competitionStage) {

            case ALL_GAMES:
                p2 = "r";
                break;
            case FINAL:
                p2 = "c";
                break;
            case GROUP:
                p2 = "r";
                break;
            case LAST_GAMES:
                p2 = "l";
                break;
            case PREPHASE_GROUP:
                p2 = "r";
                break;
            case PREPHASE_ROUND:
                p2 = "p";
                break;
            case QUALIFICATION:
                p2 = "q";
                break;
            case TRANSITIONAL:
                break;
            default:
                break;
        }

        if (competitionCode.equals("cPM")) {

            urlString = URL_COMPETITION_TRANSITIONAL;

        } else if (competitionCode.equals("rfg")) {

            urlString = URL_COMPETITION_FRIENDLY;
            p1 = competitionCode.substring(1);
            if (p2.equals("r")) {
                p2 = "g";
            }

        } else if (WORLD_COMPETITIONS.indexOf(competitionCode) != -1) {

            urlString = URL_COMPETITION_WORLD;
            if (p2.equals("c")) {
                p2 = "w";
            }

        } else if (WORLD_U21_COMPETITIONS.indexOf(competitionCode) != -1) {

            urlString = URL_COMPETITION_WORLD_U21;

        } else if (INTERNATIONAL_COMPETITIONS.indexOf(competitionCode) != -1) {

            urlString = URL_COMPETITION_INTERNATIONAL;
            p1 = competitionCode.substring(1);

        } else if (competitionCode.matches("r\\d+")) {

            urlString = URL_COMPETITION_BUSINESS_CUP;
            p1 = competitionCode.substring(1);

        } else if (competitionCode.startsWith("c")) {
            //NATIONAL CUP
            urlString = URL_COMPETITION_NATIONAL;
            p1 = competitionCode.substring(1);
            p2 = "c";

        } else {
            urlString = URL_COMPETITION_NATIONAL;
            p1 = competitionCode.substring(1);//remove "r" from beginning
        }
        //set parameters
        if (p1 != null && !p1.isEmpty()) {
            urlString = urlString.replaceAll("<1>", p1);
        }

        if (p2 != null && !p2.isEmpty()) {
            urlString = urlString.replaceAll("<2>", p2);
        }
        //System.out.println(urlString);
        return urlString;
    }

}