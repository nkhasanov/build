package com.fa13.build.model.game;

import com.fa13.build.model.PlayerAmplua;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class PlayerSubstitution extends PlayerPosition {
    private int time;
    private int minDifference;
    private int maxDifference;
    private int substitutedPlayer;
    private PlayerAmplua amplua;
    private boolean useCoordinates;

    public PlayerSubstitution() {
        markEmpty();
    }

    public boolean isEmpty() {
        return this.time == 0 && this.getNumber() == 0 && this.substitutedPlayer == 0;
    }

    public void markEmpty() {
        this.time = 0;
        this.minDifference = -20;
        this.maxDifference = 20;
        this.substitutedPlayer = 0;
        this.amplua = null;
        this.useCoordinates = false;
        setNumber(0);
        setSpecialRole(0);
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getMinDifference() {
        return minDifference;
    }

    public void setMinDifference(int minDifference) {
        this.minDifference = minDifference;
    }

    public int getMaxDifference() {
        return maxDifference;
    }

    public void setMaxDifference(int maxDifference) {
        this.maxDifference = maxDifference;
    }

    public int getSubstitutedPlayer() {
        return substitutedPlayer;
    }

    public void setSubstitutedPlayer(int substitutedPlayer) {
        this.substitutedPlayer = substitutedPlayer;
    }

    public PlayerAmplua getAmplua() {
        if (useCoordinates) {
            return super.getAmplua();
        } else {
            return amplua;
        }
    }

    public void setAmplua(PlayerAmplua amplua) {
        this.amplua = amplua;
    }

    public boolean isUseCoordinates() {
        return useCoordinates;
    }

    public void setUseCoordinates(boolean useCoordinates) {
        this.useCoordinates = useCoordinates;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
    
    public void copyFrom(PlayerSubstitution other) {
        setActOnFreekick(other.getActOnFreekick());
        setAmplua(other.getAmplua());
        setAttackX(other.getAttackX());
        setAttackY(other.getAttackY());
        setCaptain(other.isCaptain());
        setCaptainAssistant(other.isCaptainAssistant());
        setDefenceX(other.getDefenceX());
        setDefenceY(other.getDefenceY());
        setDefenderAttack(other.isDefenderAttack());
        setDirectFreekick(other.isDirectFreekick());
        setDirectFreekickAssistant(other.isDirectFreekickAssistant());
        setDispatcher(other.isDispatcher());
        setFantasista(other.isFantasista());
        setFreekickX(other.getFreekickX());
        setFreekickY(other.getFreekickY());
        setGoalkeeper(other.isGoalkeeper());
        setHardness(other.getHardness());
        setIndirectFreekick(other.isIndirectFreekick());
        setIndirectFreekickAssistant(other.isIndirectFreekickAssistant());
        setKeepBall(other.isKeepBall());
        setLeftCorner(other.isLeftCorner());
        setLeftCornerAssistant(other.isLeftCornerAssistant());
        setLongShot(other.isLongShot());
        setMaxDifference(other.getMaxDifference());
        setMinDifference(other.getMinDifference());
        setNumber(other.getNumber());
        setPassingStyle(other.getPassingStyle());
        setPenalty(other.isPenalty());
        setPenaltyAssistant(other.isPenaltyAssistant());
        setPenaltyOrder(other.getPenaltyOrder());
        setPersonalDefence(other.getPersonalDefence());
        setPressing(other.isPressing());
        setRightCorner(other.isRightCorner());
        setRightCornerAssistant(other.isRightCornerAssistant());
        setSpecialRole(other.getSpecialRole());
        setSubstitutedPlayer(other.getSubstitutedPlayer());
        setTime(other.getTime());
        setUseCoordinates(other.isUseCoordinates());
    }
}
