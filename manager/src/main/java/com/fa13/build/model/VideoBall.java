package com.fa13.build.model;

public class VideoBall implements VideoPrimitive {

    private int x = 0;
    private int y = 0;
    private int height = 0;

    public VideoBall(int x, int y, int height) {
        this.x = x;
        this.y = y;
        this.height = height;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getHeight() {
        return height;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
