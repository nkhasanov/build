package com.fa13.build.model;

public class VideoPlayer implements VideoPrimitive {
    private int x = 0;
    private int y = 0;

    public VideoPlayer() {
        super();
    }

    public VideoPlayer(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

}
