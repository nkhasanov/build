package com.fa13.build.model.game;


public enum FreekickAction {
    FK_DEFAULT("0"),
    FK_YES("1"),
    FK_NO("2");

    private final String formValue;

    private FreekickAction(String formValue) {
        this.formValue = formValue;
    }

    public static FreekickAction resolveByFormValue(String formValue) {
        for (FreekickAction action : values()) {
            if (action.formValue.equals(formValue)) {
                return action;
            }
        }
        return null;
    }

    public String getFormValue() {
        return formValue;
    }

    @Override
    public String toString() {
        return this.name();
    }
}